<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('store_type', function () {
    $shops = \Modules\Shop\Models\Shop::all();
    foreach ($shops as $shop) {
        if (is_null($shop->store_type)) {
            continue;
        }
        if (mb_stristr($shop->store_type, 'Organized')) {
            $this->info($shop->store_type . " - OR");
            $shop->update([
                'store_type' => 'OR',
            ]);
        }

        if (mb_stristr($shop->store_type, 'Independent')) {
            $this->info($shop->store_type . " - IR");
            $shop->update([
                'store_type' => 'IR',
            ]);
        }
    }
});
