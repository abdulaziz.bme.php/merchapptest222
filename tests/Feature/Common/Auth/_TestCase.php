<?php

namespace Tests\Feature\Common\Auth;

use Tests\PostmanTestCase;

class _TestCase extends PostmanTestCase
{
    protected string $requestUrl = 'auth';
}
