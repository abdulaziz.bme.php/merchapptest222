<?php

namespace Tests\Feature\Common\Information;

use Tests\PostmanTestCase;

class _TestCase extends PostmanTestCase
{
    protected string $requestUrl = 'information';
}
