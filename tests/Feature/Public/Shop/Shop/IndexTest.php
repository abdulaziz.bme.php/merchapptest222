<?php

namespace Tests\Feature\Public\Shop\Shop;

use App\Tests\Feature\Traits\Index\PaginationTrait;
use App\Tests\Feature\Traits\Index\ShowDeletedTrait;

class IndexTest extends _TestCase
{
    use PaginationTrait;

    protected string $requestMethod = self::REQUEST_METHOD_GET;

    public function test_available_relations()
    {
        $this->requestQuery = [
            'with' => [
                'city',
                'city.region',
                'company',
                'last_report',
                'suppliers',
                'contacts',
                'brands',
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(206);
    }

    public function test_available_filters()
    {
        $this->requestQuery = [
            'filter' => [
                'common' => '1',
                'id' => '1',
                'city_id' => '1',
                'company_id' => '1',
                'name' => 'shop',
                'address' => 'address',
                'has_credit_line' => '1',
                'number' => 'Number 1',
                'focus_code' => '0',
                'is_active' => '1',
                
                'city.region_id' => '1',
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(206);
    }

    public function test_available_sortings()
    {
        $this->requestQuery = [
            'sort' => [
                'id',
                'city_id',
                'company_id',
                'name',
                'address',
                'has_credit_line',
                'number',
                'is_active',
            ],
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(206);
    }
}
