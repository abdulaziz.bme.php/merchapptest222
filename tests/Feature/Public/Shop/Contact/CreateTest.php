<?php

namespace Tests\Feature\Public\Shop\Contact;

use Illuminate\Http\UploadedFile;

class CreateTest extends _TestCase
{
    protected string $requestMethod = self::REQUEST_METHOD_POST;

    public function test_success()
    {
        $this->requestBody = [
            'full_name' => 'Full name 4',
            'position' => 'Position 4',
            'phone' => 'Phone 4',
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(201);
    }
}
