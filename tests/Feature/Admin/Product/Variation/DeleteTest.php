<?php

namespace Tests\Feature\Admin\Product\Variation;

use App\Tests\Feature\Traits\Delete\DeleteTrait;
use App\Tests\Feature\Traits\Delete\RestoreTrait;

class DeleteTest extends _TestCase
{
    use DeleteTrait;
    use RestoreTrait;
}
