<?php

namespace Tests\Feature\Admin\User\Profile;

use Illuminate\Http\UploadedFile;

class UpdateTest extends _TestCase
{
    protected string $requestMethod = self::REQUEST_METHOD_POST;

    public function test_success()
    {
        $this->requestBody = [
            'email' => 'admin@local.host',
            'full_name' => 'Administrator',
            'phone' => '001111111',
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(200);
    }

    public function test_change_password()
    {
        $this->requestBody = [
            'new_password' => 'vu8eaajiaw',
        ];

        $this->response = $this->sendRequest();
        $this->response->assertStatus(200);
    }
}
