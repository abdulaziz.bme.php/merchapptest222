<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Product\Models\ProductVariation;

class RunCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larbox:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run script';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $variations = ProductVariation::query()->with(['options'])->get();

        foreach ($variations as $variation) {
            $variation->save();
        }
    }
}
