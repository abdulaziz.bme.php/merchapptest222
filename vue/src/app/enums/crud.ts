export enum valueTypes {
    text,
    boolean,
    image,
    array,
    httpSelect,
    httpSwitcher,
    relations,
    component,
}
