export default [
  {
    label: 'Главная',
    icon: 'fas fa-tachometer-alt',
    path: '',
  },
  {
    label: 'Пользователи',
    icon: 'fas fa-users',
    path: 'user/user',
  },
  {
    label: 'Компании',
    icon: 'fas fa-users',
    path: 'companies',
  },
  {
    label: 'Отчёты',
    icon: 'fas fa-clipboard-list',
    path: 'report/report',
  },
  {
    label: 'Задачи',
    icon: 'fas fa-tasks',
    path: 'task/task',
  },
  {
    label: 'Продукты',
    icon: 'fas fa-boxes',
    path: 'product',
    children: [
      {
        label: 'Продукты',
        path: 'product/product',
      },
      {
        label: 'Категории',
        path: 'product/category',
      },
      {
        label: 'Бренды',
        path: 'product/brand',
      },
      {
        label: 'Характеристики',
        path: 'product/specification',
      },
    ],
  },
  {
    label: 'Регионы',
    icon: 'fas fa-city',
    path: 'region',
    children: [
      {
        label: 'Регионы',
        path: 'region/region',
      },
      {
        label: 'Города',
        path: 'region/city',
      },
    ],
  },
  {
    label: 'Магазины',
    icon: 'fas fa-store',
    path: 'shop',
    children: [
      {
        label: 'Магазины',
        path: 'shop/shop',
      },
      {
        label: 'Компании',
        path: 'shop/company',
      },
      {
        label: 'Контакты',
        path: 'shop/contact',
      },
      {
        label: 'Поставщики',
        path: 'shop/supplier',
      },
    ],
  },
  {
    label: 'Импорт / экспорт',
    icon: 'fas fa-file-alt',
    path: 'exchange',
    children: [
      {
        label: 'EXCEL импорт',
        path: 'exchange/excel/import',
      },
      {
        label: 'EXCEL экспорт',
        path: 'exchange/excel/export',
      },
    ],
  },
  {
    label: 'Система',
    icon: 'fas fa-cogs',
    path: 'system',
    children: [
      {
        label: 'Настройки',
        path: 'system/settings',
      },
      {
        label: 'Языки',
        path: 'system/language',
      },
    ],
  }, {
    label: 'Вопросы',
    icon: 'fas fa-question',
    path: 'questions',
    children: [
      {
        label: 'Группы',
        path: 'questions/groups',
      },{
        label: 'Ответы',
        path: 'questions/answers',
      }
    ],
  },
];
