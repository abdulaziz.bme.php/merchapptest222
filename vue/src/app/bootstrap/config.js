let authUsername = localStorage.getItem('auth_username') ?? '',
    authPassword = localStorage.getItem('auth_password') ?? '';

export default {
    http: {
        url: {
            common: window.__VUE_OPTIONS_API__ ? 'http://bm-electronics.loc/api' : 'https://testapi.merchapp.uz/api',
            main: null,
        },
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Basic ' + window.btoa(authUsername + ':' + authPassword),
        },
    },
};
