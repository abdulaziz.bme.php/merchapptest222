import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';
import TextInput from "@/modules/product/components/TextInput.vue";

export default new CrudModel({
  showDeleted: true,

  list: {
    position_number: {
      value: 'position_number',
      cellAttributes: {style: "width:100px;"},
      type: crudEnums.valueTypes.component,
      options: {
        resolve: (context, item) => {
          return {
            component: TextInput,
            params: [
              {
                value: item.value,
                options: {hideLabel: true},
                wrapperSize: "",
                name: "position_number[" + item.item.id + "]",
                item: item.item,
              }
            ],
          };
        },
      },
    },
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    creator_id: {
      value: 'creator.username',
      type: crudEnums.valueTypes.text,
    },
    name: {
      value: 'name',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.httpSwitcher,
      options: {
        path: 'product/brand/:id/set-active/:value',
      },
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    id: {
      type: inputEnums.types.number,
    },
    creator_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'user/user',
        field: 'username',
      },
    },
    name: {
      type: inputEnums.types.text,
    },
    is_active: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
  },

  sortings: [
    'id',
    'name',
  ],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    creator_id: {
      value: 'creator.username',
      type: crudEnums.valueTypes.text,
    },
    name: {
      value: 'name',
      type: crudEnums.valueTypes.text,
    },
    is_active: {
      value: 'is_active',
      type: crudEnums.valueTypes.boolean,
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  form: {
    'Информация': {
      name: {
        type: inputEnums.types.text,
      },
    },
  },
});
