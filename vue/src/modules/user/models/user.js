import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

export default new CrudModel({
  showDeleted: true,

  list: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    username: {
      value: 'username',
      type: crudEnums.valueTypes.text,
    },
    email: {
      value: 'email',
      type: crudEnums.valueTypes.text,
    },
    'user.full_name': {
      value: 'full_name',
      type: crudEnums.valueTypes.text,
    },
    phone: {
      value: 'phone',
      type: crudEnums.valueTypes.text,
    },
    role: {
      value: 'role',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => context.booted.enums.user.role[value].label,
      },
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    common: {
      label: 'Общий поиск',
      hint: (context) => {
        let fields = ['username', 'email', 'user.full_name', 'phone'];

        return context.__('Поиск по полям: :fields', {
          fields: fields.map((value) => context.__('fields->' + value)).join(' | '),
        });
      },
      type: inputEnums.types.text,
      wrapperSize: inputEnums.wrapperSizes.xl,
    },
    id: {
      type: inputEnums.types.number,
    },
    role: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return Object.fromEntries(
            Object.entries(context.booted.enums.user.role).map((entry) => {
              entry[1] = entry[1].label;
              return entry;
            }),
          );
        },
        withPrompt: true,
      },
    },
  },

  sortings: [
    'id',
    'username',
    'full_name',
  ],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    username: {
      value: 'username',
      type: crudEnums.valueTypes.text,
    },
    email: {
      value: 'email',
      type: crudEnums.valueTypes.text,
    },
    'user.full_name': {
      value: 'full_name',
      type: crudEnums.valueTypes.text,
    },
    phone: {
      value: 'phone',
      type: crudEnums.valueTypes.text,
    },
    role: {
      value: 'role',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => context.booted.enums.user.role[value].label,
      },
    },
    regions: {
      value: 'regions',
      type: crudEnums.valueTypes.array,
      options: {
        path: 'name.:locale',
      },
    },
    cities: {
      value: 'cities',
      type: crudEnums.valueTypes.array,
      options: {
        path: 'name.:locale',
      },
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
  },

  form: {
    'Информация': {
      username: {
        options: {
          required: true
        },
        type: inputEnums.types.text,
      },
      email: {
        type: inputEnums.types.text,
      },
      full_name: {
        options: {
          required: true
        },
        label: 'user.full_name',
        type: inputEnums.types.text,
      },
      phone: {
        options: {
          required: true
        },
        type: inputEnums.types.phone,
      },
      role: {
        type: inputEnums.types.select,
        options: {
          required: true,
          items: (context) => {
            return Object.fromEntries(
              Object.entries(context.booted.enums.user.role).map((entry) => {
                entry[1] = entry[1].label;
                return entry;
              }),
            );
          },
          withPrompt: true,
        },
      },
      new_password: {
        options:{
          required: true,
        },
        type: inputEnums.types.password,
      },
      regions: {
        value: (value) => Object.values(value).map((value) => value.id),
        select2Value: 'regions',
        type: inputEnums.types.select2Ajax,
        options: {
          required: true,
          path: 'region/region',
          field: 'name:locale',
          isMultiple: true,
          select2SubValue: 'name.:locale',
        },
      },
      cities: {
        value: (value) => Object.values(value).map((value) => value.id),
        select2Value: 'cities',
        type: inputEnums.types.select2Ajax,
        options: {
          required: true,
          path: 'region/city',
          field: 'name:locale',
          isMultiple: true,
          select2SubValue: 'name.:locale',
        },
      },
    },
  },

  formInit: () => {
    if ($('#crud-form input').length === 0) return;

    let $roleEl = $('#crud-form [name="role"]'),
      $regionsEl = $('#crud-form [name="regions[]"]'),
      $citiesEl = $('#crud-form [name="cities[]"]');

    $roleEl.on('change', () => {
      switch ($roleEl.val()) {
        case 'admin':
          $regionsEl.closest('.input-wrapper').addClass('d-none');
          $citiesEl.closest('.input-wrapper').addClass('d-none');
          break;
        case 'agent':
          $regionsEl.closest('.input-wrapper').removeClass('d-none');
          $citiesEl.closest('.input-wrapper').removeClass('d-none');
          break;
      }
    });

    $roleEl.trigger('change');
  },
});
