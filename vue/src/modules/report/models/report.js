import {CrudModel} from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

import RouterLink from '@/app/components/vue/RouterLink.vue';

export default new CrudModel({
  showDeleted: true,

  list: {
    images_list: {
      value: 'images_list.0.w_100',
      type: crudEnums.valueTypes.image,
      options: {
        hideLabel: true,
      },
    },
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    creator_id: {
      value: 'creator.username',
      type: crudEnums.valueTypes.text,
    },
    number: {
      value: 'number',
      type: crudEnums.valueTypes.text,
    },
    type: {
      value: 'type',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => context.booted.enums.report.type[value].label,
      },
    },
    shop_id: {
      value: 'shop.number',
      type: crudEnums.valueTypes.text,
    },
    shop_is_closed: {
      html: true,
      value: 'shop_is_closed',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => value ? "<span class='text-danger'>Закрыто</span>" : "<span class='text-success'>Открыто</span>",
      },
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
  },

  filters: {
    id: {
      type: inputEnums.types.number,
    },
    creator_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'user/user',
        field: 'username',
      },
    },
    type: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return Object.fromEntries(
            Object.entries(context.booted.enums.report.type).map((entry) => {
              entry[1] = entry[1].label;
              return entry;
            }),
          );
        },
        withPrompt: true,
      },
    },
    shop_id: {
      type: inputEnums.types.select2Ajax,
      options: {
        path: 'shop/shop',
        field: 'number',
      },
    },
    shop_is_closed: {
      type: inputEnums.types.select,
      options: {
        items: (context) => {
          return {
            0: context.__('Нет'),
            1: context.__('Да'),
          };
        },
        withPrompt: true,
      },
    },
    year: {
      type: inputEnums.types.text,
    },
    date_period_min: {
      type: inputEnums.types.date,
    },
    date_period_max: {
      type: inputEnums.types.date,
    },
  },

  sortings: ['id', 'date_period_from', 'date_period_to'],

  show: {
    id: {
      value: 'id',
      type: crudEnums.valueTypes.text,
    },
    task_id: {
      value: 'task_report.task_id',
      type: crudEnums.valueTypes.component,
      options: {
        resolve: (context, item) => {
          return {
            component: RouterLink,
            params: [
              {
                to: 'task/task/' + item.value + '/show',
                target: '_blank',
              },
              () => context.__('Ссылка'),
            ],
          };
        },
      },
    },
    creator_id: {
      value: 'creator.username',
      type: crudEnums.valueTypes.text,
    },
    number: {
      value: 'number',
      type: crudEnums.valueTypes.text,
    },
    type: {
      value: 'type',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => context.booted.enums.report.type[value].label,
      },
    },
    shop_id: {
      value: 'shop.number',
      type: crudEnums.valueTypes.text,
    },
    shop_is_closed: {
      html: true,
      value: 'shop_is_closed',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => value ? "<span class='text-danger'>Закрыто</span>" : "<span class='text-success'>Открыто</span>",
      },
    },
    date_period_type: {
      value: 'date_period_type',
      type: crudEnums.valueTypes.text,
      options: {
        onComplete: (context, value) => context.booted.enums.report.date_period_type[value].label,
      },
    },
    date_period_interval: {
      value: 'date_period_interval',
      type: crudEnums.valueTypes.text,
    },
    location: {
      value: 'location',
      type: crudEnums.valueTypes.component,
      options: {
        resolve: (context, item) => {
          // let link = 'https://yandex.uz/maps/?ll=65.104751%2C42.543778&mode=whatshere&whatshere%5Bpoint%5D=';

          let mapCoors = [parseFloat(item.value[0]) + 0.045, parseFloat(item.value[1]) + 0.009];

          let link = 'https://yandex.uz/maps/?ll=';

          link += mapCoors[1] + '%2C' + mapCoors[0];
          link += '&mode=whatshere&whatshere%5Bpoint%5D=';
          link += item.value[1] + '%2C' + item.value[0];
          link += '&whatshere%5Bzoom%5D=15.4&z=12.73';

          return {
            component: 'a',
            params: [
              {
                href: link,
                target: '_blank',
                innerHTML: context.__('Ссылка'),
              },
            ],
          };
        },
      },
    },
    comment: {
      value: 'comment',
      type: crudEnums.valueTypes.text,
    },
    created_at: {
      value: 'created_at',
      type: crudEnums.valueTypes.text,
    },
    updated_at: {
      value: 'updated_at',
      type: crudEnums.valueTypes.text,
    },
    products: {
      value: 'products',
      type: crudEnums.valueTypes.relations,
      options: {
        fields: {
          product_id: {
            value: 'product.model_number',
            type: crudEnums.valueTypes.text,
          },
          variation_id: {
            value: 'variation.name',
            type: crudEnums.valueTypes.text,
          },
          supplier_id: {
            value: 'supplier.short_name',
            type: crudEnums.valueTypes.text,
          },
          quantity: {
            value: 'quantity',
            type: crudEnums.valueTypes.text,
          },
          price: {
            value: 'price',
            type: crudEnums.valueTypes.text,
          },
        },
      },
    },
    brands: {
      value: 'brands',
      type: crudEnums.valueTypes.relations,
      options: {
        fields: {
          brand_id: {
            value: 'brand.name',
            type: crudEnums.valueTypes.text,
          },
          quantity: {
            value: 'quantity',
            type: crudEnums.valueTypes.text,
          },
        },
      },
    },
    images_list: {
      value: 'images_list',
      type: crudEnums.valueTypes.image,
      options: {
        isMultiple: true,
        path: 'w_500',
      },
    },
  },

  form: {
    'Информация': {
      creator_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'creator.username',
        options: {
          path: 'user/user',
          field: 'username',
        },
      },
      type: {
        type: inputEnums.types.select,
        options: {
          items: (context) => {
            return Object.fromEntries(
              Object.entries(context.booted.enums.report.type).map((entry) => {
                entry[1] = entry[1].label;
                return entry;
              }),
            );
          },
          withPrompt: true,
        },
      },
      date_period_type: {
        type: inputEnums.types.select,
        options: {
          items: (context) => {
            return Object.fromEntries(
              Object.entries(context.booted.enums.report.date_period_type).map((entry) => {
                entry[1] = entry[1].label;
                return entry;
              }),
            );
          },
          withPrompt: true,
        },
      },
      date_period_from: {
        type: inputEnums.types.date,
      },
      shop_id: {
        type: inputEnums.types.select2Ajax,
        select2Value: 'shop.number',
        options: {
          path: 'shop/shop',
          field: 'number',
          onChange: ($el) => {
            $('[name*="[supplier_id]"]').attr('disabled', $el.val() === '');
          },
        },
      },
      shop_is_closed: {
        type: inputEnums.types.switcher,
      },
      'location.0': {
        type: inputEnums.types.text,
      },
      'location.1': {
        type: inputEnums.types.text,
      },
      images_list: {
        type: inputEnums.types.file,
        options: {
          previewPath: 'w_500',
          downloadPath: 'original',
          // deletePath: 'report/report/:id/delete-file/images_list/:index',
          isMultiple: true,
        },
        wrapperSize: inputEnums.wrapperSizes.xl,
      },
      comment: {
        type: inputEnums.types.textarea,
        wrapperSize: inputEnums.wrapperSizes.xl,
      },
    },
    'Продукты': {
      products: {
        type: inputEnums.types.relations,
        options: {
          fields: {
            'product.brand_id': {
              type: inputEnums.types.select2Ajax,
              select2Value: 'product.brand.name',
              options: {
                path: 'product/brand',
                field: 'name',
                onChange: ($el) => {
                  $el.closest('.crud-relation')
                    .find('[name*="product_id"]')
                    .attr('disabled', $el.val() === '');
                  $el.closest('.crud-relation')
                    .find('[name*="variation_id"]')
                    .attr('disabled', $el.val() === '');
                },
              },
            },
            product_id: {
              type: inputEnums.types.select2Ajax,
              select2Value: 'product.model_number',
              options: {
                path: 'product/product',
                query: ($el) => {
                  let id = $el.closest('.crud-relation').find('[name*="brand_id"]').val();
                  return {'filter[brand_id]': id};
                },
                field: 'model_number',
                onChange: ($el) => {
                  $el.closest('.crud-relation')
                    .find('[name*="variation_id"]')
                    .attr('disabled', $el.val() === '');
                },
              },
            },
            variation_id: {
              type: inputEnums.types.select2Ajax,
              select2Value: 'variation.name',
              options: {
                path: 'product/variation',
                query: ($el) => {
                  let id,
                    result = {'with[0]': 'options'};

                  id = $el.closest('.crud-relation').find('[name*="product_id"]').val();
                  result['filter[product_id]'] = id;

                  return result;
                },
                field: 'options.name',
                valueField: 'name',
              },
            },
            supplier_id: {
              type: inputEnums.types.select2Ajax,
              select2Value: 'supplier.full_name',
              options: {
                path: 'shop/supplier',
                field: 'full_name',
              },
            },
            quantity: {
              type: inputEnums.types.text,
            },
            price: {
              type: inputEnums.types.text,
            },
          },
        },
      },
    },
    'Бренды': {
      brands: {
        type: inputEnums.types.relations,
        options: {
          fields: {
            brand_id: {
              type: inputEnums.types.select2Ajax,
              select2Value: 'brand.name',
              options: {
                path: 'product/brand',
                field: 'name',
              },
            },
            quantity: {
              type: inputEnums.types.text,
            },
          },
        },
      },
    },
  },
});
