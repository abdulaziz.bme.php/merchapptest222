export function sendRequest(context, event, path) {
    let formData = new FormData(event.target);

    context.booted.helpers.http
        .send(context, {
            method: 'POST',
            path: path,
            body: formData,
        })
        .then((response) => {
            if (response.statusType === 'success') {
                response.json().then((body) => {
                    toastr.success(body.message);

                    context.booted.components.app.$data.appChildKey++;
                    context.$router.push(context.$router.currentRoute);
                });
            } else if (response.statusType === 'validationFailed') {
                response.json().then((body) => {
                    toastr.warning(body.message);

                    $('.input-error').addClass('d-none');

                    for (let key in body.errors) {
                        let error = body.errors[key].join('\n'),
                            altKey = null;

                        if (key.includes('.')) {
                            altKey = key.slice(0, key.lastIndexOf('.')) + '.*';
                        } else {
                            altKey = key + '.*';
                        }

                        $('[data-error-key="' + key + '"], [data-error-key="' + altKey + '"]')
                            .closest('.input-wrapper')
                            .find('.input-error')
                            .text(error)
                            .removeClass('d-none');
                    }
                });
            }
        });
}
