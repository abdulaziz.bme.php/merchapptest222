export default [
    {
        path: 'shop',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
            {
                path: 'shop',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/shop/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/shop/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/shop/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/shop/Update.vue"),
                    },
                ],
            },
            {
                path: 'company',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/company/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/company/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/company/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/company/Update.vue"),
                    },
                ],
            },
            {
                path: 'contact',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/contact/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/contact/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/contact/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/contact/Update.vue"),
                    },
                ],
            },
            {
                path: 'supplier',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/supplier/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/supplier/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/supplier/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/supplier/Update.vue"),
                    },
                ],
            },
        ],
    },
];
