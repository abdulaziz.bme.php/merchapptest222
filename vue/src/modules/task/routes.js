export default [
    {
        path: 'task',
        component: () => import("@/app/views/EmptyDecorator.vue"),
        children: [
            {
                path: 'task',
                component: () => import("@/app/views/EmptyDecorator.vue"),
                children: [
                    {
                        path: '',
                        component: () => import("./views/task/Index.vue"),
                    },
                    {
                        path: ':id/show',
                        component: () => import("./views/task/Show.vue"),
                    },
                    {
                        path: 'create',
                        component: () => import("./views/task/Create.vue"),
                    },
                    {
                        path: ':id/update',
                        component: () => import("./views/task/Update.vue"),
                    },
                ],
            },
        ],
    },
];
