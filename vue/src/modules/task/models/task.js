import { CrudModel } from '@/app/core/models/model';

import * as crudEnums from '@/app/enums/crud';
import * as inputEnums from '@/app/enums/input';

import RouterLink from '@/app/components/vue/RouterLink.vue';

export default new CrudModel({
    showDeleted: true,

    list: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name',
            type: crudEnums.valueTypes.text,
        },
        deadline: {
            value: 'deadline',
            type: crudEnums.valueTypes.text,
        },
        agent_id: {
            value: 'agent.username',
            type: crudEnums.valueTypes.text,
        },
        type: {
            value: 'type',
            type: crudEnums.valueTypes.text,
            options: {
                onComplete: (context, value) => context.booted.enums.task.type[value].label,
            },
        },
        agent_status: {
            value: 'agent_status',
            type: crudEnums.valueTypes.text,
            options: {
                onComplete: (context, value) => context.booted.enums.task.agent_status[value].label,
            },
        },
        admin_status: {
            value: 'admin_status',
            type: crudEnums.valueTypes.httpSelect,
            options: {
                items: (context, value) => {
                    return Object.fromEntries(
                        Object.entries(context.booted.enums.task.admin_status).map((entry) => {
                            entry[1] = entry[1].label;
                            return entry;
                        }),
                    );
                },
                path: 'task/task/:id/set-admin_status/:value',
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    filters: {
        id: {
            type: inputEnums.types.number,
        },
        name: {
            type: inputEnums.types.text,
        },
        deadline: {
            type: inputEnums.types.date,
        },
        agent_id: {
            type: inputEnums.types.select2Ajax,
            options: {
                path: 'user/user',
                query: {
                    'filter[role]': 'agent',
                },
                field: 'username',
            },
        },
        type: {
            type: inputEnums.types.select,
            options: {
                items: (context) => {
                    return Object.fromEntries(
                        Object.entries(context.booted.enums.task.type).map((entry) => {
                            entry[1] = entry[1].label;
                            return entry;
                        }),
                    );
                },
                withPrompt: true,
            },
        },
        agent_status: {
            type: inputEnums.types.select,
            options: {
                items: (context) => {
                    return Object.fromEntries(
                        Object.entries(context.booted.enums.task.agent_status).map((entry) => {
                            entry[1] = entry[1].label;
                            return entry;
                        }),
                    );
                },
                withPrompt: true,
            },
        },
        admin_status: {
            type: inputEnums.types.select,
            options: {
                items: (context) => {
                    return Object.fromEntries(
                        Object.entries(context.booted.enums.task.admin_status).map((entry) => {
                            entry[1] = entry[1].label;
                            return entry;
                        }),
                    );
                },
                withPrompt: true,
            },
        },
    },

    sortings: [
        'id',
        'name',
        'deadline',
    ],

    show: {
        id: {
            value: 'id',
            type: crudEnums.valueTypes.text,
        },
        name: {
            value: 'name',
            type: crudEnums.valueTypes.text,
        },
        description: {
            value: 'description',
            type: crudEnums.valueTypes.text,
        },
        execution_comment: {
            value: 'execution_comment',
            type: crudEnums.valueTypes.text,
        },
        deadline: {
            value: 'deadline',
            type: crudEnums.valueTypes.text,
        },
        agent_id: {
            value: 'agent.username',
            type: crudEnums.valueTypes.text,
        },
        type: {
            value: 'type',
            type: crudEnums.valueTypes.text,
            options: {
                onComplete: (context, value) => context.booted.enums.task.type[value].label,
            },
        },
        agent_status: {
            value: 'agent_status',
            type: crudEnums.valueTypes.text,
            options: {
                onComplete: (context, value) => context.booted.enums.task.agent_status[value].label,
            },
        },
        admin_status: {
            value: 'admin_status',
            type: crudEnums.valueTypes.text,
            options: {
                onComplete: (context, value) => context.booted.enums.task.admin_status[value].label,
            },
        },
        reports: {
            value: 'reports',
            type: crudEnums.valueTypes.relations,
            options: {
                fields: {
                    shop_id: {
                        value: 'shop.number',
                        type: crudEnums.valueTypes.text,
                    },
                    type: {
                        value: 'type',
                        type: crudEnums.valueTypes.text,
                        options: {
                            onComplete: (context, value) => context.booted.enums.report.type[value].label,
                        },
                    },
                    date_period_type: {
                        value: 'date_period_type',
                        type: crudEnums.valueTypes.text,
                        options: {
                            onComplete: (context, value) => context.booted.enums.report.date_period_type[value].label,
                        },
                    },
                    report_id: {
                        value: 'report.id',
                        type: crudEnums.valueTypes.component,
                        options: {
                            resolve: (context, item) => {
                                return {
                                    component: RouterLink,
                                    params: [
                                        {
                                            to: 'report/report/' + item.value + '/show',
                                            target: '_blank',
                                            class: 'font-weight-bold',
                                        },
                                        () => context.__('Ссылка'),
                                    ],
                                };
                            },
                        },
                    },
                },
            },
        },
        created_at: {
            value: 'created_at',
            type: crudEnums.valueTypes.text,
        },
        updated_at: {
            value: 'updated_at',
            type: crudEnums.valueTypes.text,
        },
    },

    form: {
        'Информация': {
            name: {
                type: inputEnums.types.text,
            },
            deadline: {
                type: inputEnums.types.date,
            },
            description: {
                type: inputEnums.types.textarea,
            },
            execution_comment: {
                type: inputEnums.types.textarea,
            },
            agent_id: {
                type: inputEnums.types.select2Ajax,
                select2Value: 'agent.username',
                options: {
                    path: 'user/user',
                    query: {
                        'filter[role]': 'agent',
                    },
                    field: 'username',
                    onChange: ($el) => {
                        $('[name*="[shop_id]"]').attr('disabled', $el.val() === '');
                    },
                },
            },
            type: {
                type: inputEnums.types.select,
                options: {
                    items: (context) => {
                        return Object.fromEntries(
                            Object.entries(context.booted.enums.task.type).map((entry) => {
                                entry[1] = entry[1].label;
                                return entry;
                            }),
                        );
                    },
                    withPrompt: true,
                },
            },
        },
        'Бланки для отчётов': {
            reports: {
                type: inputEnums.types.relations,
                options: {
                    fields: {
                        shop_id: {
                            type: inputEnums.types.select2Ajax,
                            select2Value: 'shop.number',
                            options: {
                                path: 'shop/shop',
                                query: () => {
                                    let id = $('[name="agent_id"]').val();
                                    return { 'filter[agent_id]': id };
                                },
                                field: 'number',
                            },
                            wrapperSize: inputEnums.wrapperSizes.md,
                        },
                        type: {
                            type: inputEnums.types.select,
                            options: {
                                items: (context) => {
                                    return Object.fromEntries(
                                        Object.entries(context.booted.enums.report.type).map((entry) => {
                                            entry[1] = entry[1].label;
                                            return entry;
                                        }),
                                    );
                                },
                                withPrompt: true,
                            },
                            wrapperSize: inputEnums.wrapperSizes.md,
                        },
                        date_period_type: {
                            type: inputEnums.types.select,
                            options: {
                                items: (context) => {
                                    return Object.fromEntries(
                                        Object.entries(context.booted.enums.report.date_period_type).map((entry) => {
                                            entry[1] = entry[1].label;
                                            return entry;
                                        }),
                                    );
                                },
                                withPrompt: true,
                            },
                            wrapperSize: inputEnums.wrapperSizes.md,
                        },
                    },
                },
            },
        },
    },
});