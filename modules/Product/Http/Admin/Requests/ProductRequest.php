<?php

namespace Modules\Product\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Modules\Product\Models\Product;
use Modules\Product\Models\ProductBrand;

use Illuminate\Validation\Rule;
use App\Rules\ExistsSoftDeleteRule;
use App\Helpers\FormRequestHelper;

class ProductRequest extends ActiveFormRequest
{
    protected array $ignoredModelFields = ['images_list'];
    protected bool $hasDateFolderForFiles = false;
    protected array $fileFields = [
        'images_list' => 'images',
    ];

    public function __construct()
    {
        return parent::__construct(
            model: new Product()
        );
    }

    public function rules()
    {
        return [
            'category_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product_category'),
            ],
            'brand_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product_brand'),
            ],

            'model_number' => [
                'required',
                Rule::unique($this->model->getTable())->ignore($this->model->id)->where('brand_id', $this->brand_id),
            ],
            'date_eol' => 'nullable|date|date_format:d.m.Y',

            'images_list' => 'array',
            'images_list.*' => 'file|max:4096|mimes:jpg,png,webp',
        ];
    }

    protected  function passedValidation()
    {
        parent::passedValidation();

        $brandName = ProductBrand::query()->findOrFail($this->brand_id)->name;

        $this->fileFields['images_list'] = "../exchange/product/$brandName/$this->model_number";
    }
}
