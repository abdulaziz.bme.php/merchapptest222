<?php

namespace Modules\Product\Search;

use App\Search\Search;

class ProductVariationSearch extends Search
{
    protected array $relations = [
        'options',
    ];

    protected array $filterable = [
        'id' => Search::FILTER_TYPE_IN,
        'product_id' => Search::FILTER_TYPE_EQUAL,
        'sku' => Search::FILTER_TYPE_EQUAL_INSENSITIVE,

        'options.name' => Search::FILTER_TYPE_LOCALIZED,
    ];

    protected array $combinedFilterable = [
        'common' => [
            'type' => Search::COMBINED_TYPE_OR,
            'fields' => [
                'sku' => Search::FILTER_TYPE_EQUAL_INSENSITIVE,
                'options.name' => Search::FILTER_TYPE_LOCALIZED,
            ],
        ],
    ];

    protected array $sortable = [
        'id' => Search::SORT_TYPE_SIMPLE,
        'product_id' => Search::SORT_TYPE_SIMPLE,
        'sku' => Search::SORT_TYPE_SIMPLE,
    ];
}
