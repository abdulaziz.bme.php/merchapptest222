<?php

namespace Modules\Product\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\Report\Models\ReportProduct;
use Modules\User\Models\User;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'product';

    protected $fillable = [
        'category_id',
        'brand_id',
        'model_number',
        'date_eol',
        'images_list',
        'extra_id',
    ];

    protected $casts = [
        'date_eol'    => 'date:d.m.Y',
        'images_list' => 'array',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id')->withTrashed();
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id')->withTrashed();
    }

    public function brand()
    {
        return $this->belongsTo(ProductBrand::class, 'brand_id')->withTrashed();
    }

    public function variations()
    {
        return $this->hasMany(ProductVariation::class, 'product_id')->withTrashed();
    }

    public function report_product()
    {
        return $this->hasOne(ReportProduct::class, 'product_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->date_eol = Carbon::parse($model->date_eol)->format('Y-m-d');
        });
    }
}
