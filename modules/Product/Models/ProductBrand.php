<?php

namespace Modules\Product\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Shop\Models\Shop;
use Modules\User\Models\User;

class ProductBrand extends Model
{
    use SoftDeletes;

    protected $table = 'product_brand';

    protected $fillable = [
        'name',
        'position_number',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id')->withTrashed();
    }

    public function shops()
    {
        return $this->belongsToMany(
            Shop::class,
            'shop_brand_ref',
            'brand_id',
            'shop_id'
        )->withTrashed();
    }

    protected static function boot()
    {
        parent::boot();
        self::creating(function (self $model) {
            $latestNumber = ProductBrand::latest('position_number')->pluck('position_number')->first();
            if ($latestNumber === null)
                $latestNumber = -1;
            $model->position_number = $latestNumber + 1;
        });
    }
}
