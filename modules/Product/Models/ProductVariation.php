<?php

namespace Modules\Product\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class ProductVariation extends Model
{
    use SoftDeletes;

    protected $table = 'product_variation';

    protected $fillable = [
        'product_id',
        'sku',
        'images_list',
    ];

    protected $casts = [
        'name' => 'array',
        'images_list' => 'array',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function options()
    {
        return $this->belongsToMany(
            ProductSpecificationOption::class,
            'product_variation_option_ref',
            'variation_id',
            'option_id'
        )->orderBy('specification_id');
    }

    protected static function boot()
    {
        parent::boot();

        self::saved(function (self $model) {
            $model->name = Arr::pluck($model->options, 'name');
            $model->saveQuietly();
        });
    }
}
