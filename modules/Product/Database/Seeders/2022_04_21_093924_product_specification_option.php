<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

return new class extends Seeder
{
    public function run()
    {
        DB::table('product_specification_option')->insert([
            [
                'specification_id' => 1,
                'name' => json_encode([
                    'ru' => 'White',
                    'uz' => 'White',
                ]),
                'sort_index' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'specification_id' => 1,
                'name' => json_encode([
                    'ru' => 'Black',
                    'uz' => 'Black',
                ]),
                'sort_index' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'specification_id' => 1,
                'name' => json_encode([
                    'ru' => 'Red',
                    'uz' => 'Red',
                ]),
                'sort_index' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            
            [
                'specification_id' => 2,
                'name' => json_encode([
                    'ru' => 'Option 2-1',
                    'uz' => 'Option 2-1',
                ]),
                'sort_index' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'specification_id' => 2,
                'name' => json_encode([
                    'ru' => 'Option 2-2',
                    'uz' => 'Option 2-2',
                ]),
                'sort_index' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
};
