<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

return new class extends Seeder
{
    public function run()
    {
        DB::table('product_variation')->insert([
            [
                'product_id' => 1,
                'name' => json_encode([
                    [
                        'ru' => 'White',
                        'uz' => 'White',
                    ],
                ]),
                'sku' => 'SKU 1',
                'images_list' => json_encode([
                    '/test_data/images/1.png',
                    '/test_data/images/2.png',
                    '/test_data/images/3.png',
                ]),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'product_id' => 1,
                'name' => json_encode([
                    [
                        'ru' => 'Option 2-1',
                        'uz' => 'Option 2-1',
                    ],
                ]),
                'sku' => 'SKU 2',
                'images_list' => json_encode([
                    '/test_data/images/2.png',
                ]),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'product_id' => 2,
                'name' => json_encode([
                    [
                        'ru' => 'Black',
                        'uz' => 'Black',
                    ],
                ]),
                'sku' => 'SKU 3',
                'images_list' => json_encode([
                    '/test_data/images/3.png',
                ]),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
};
