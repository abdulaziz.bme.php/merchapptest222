<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

return new class extends Seeder
{
    public function run()
    {
        DB::table('product')->insert([
            [
                'creator_id' => 2,
                'category_id' => 1,
                'brand_id' => 1,
                'model_number' => 'Model 1',
                'date_eol' => date('Y-m-d'),
                'images_list' => json_encode([
                    '/test_data/images/1.png',
                    '/test_data/images/2.png',
                    '/test_data/images/3.png',
                ]),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'creator_id' => 2,
                'category_id' => 2,
                'brand_id' => 2,
                'model_number' => 'Model 2',
                'date_eol' => date('Y-m-d'),
                'images_list' => json_encode([
                    '/test_data/images/2.png',
                ]),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'creator_id' => 1,
                'category_id' => 1,
                'brand_id' => 1,
                'model_number' => 'Model 101',
                'date_eol' => date('Y-m-d'),
                'images_list' => json_encode([]),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'creator_id' => 2,
                'category_id' => 2,
                'brand_id' => 2,
                'model_number' => 'Model 102',
                'date_eol' => date('Y-m-d'),
                'images_list' => json_encode([
                    '/test_data/images/1.png',
                    '/test_data/images/3.png',
                ]),
                'is_active' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
};
