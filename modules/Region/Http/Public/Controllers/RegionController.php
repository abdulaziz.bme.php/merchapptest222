<?php

namespace Modules\Region\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;

use Modules\Region\Models\Region;
use Modules\Region\Search\RegionSearch;
use Modules\Region\Resources\RegionResource;
use Illuminate\Support\Arr;

class RegionController extends ApiResourceController
{
    public function __construct()
    {
        parent::__construct(
            model: new Region(),
            search: new RegionSearch(),
            resourceClass: RegionResource::class
        );

        $this->middleware(function ($request, $next) {
            $showParams = (array)request()->get('show');
            $showParams = Arr::flatten($showParams);

            if (in_array('only-my', $showParams)) {
                if ($regions = auth()->user()->regions) {
                    $regions = $regions->pluck('id')->toArray();
                    $this->search->queryBuilder->whereIn('id', $regions);
                }
            }

            return $next($request);
        });
    }
}
