<?php

use Illuminate\Support\Facades\Route;

use Modules\Region\Http\Public\Controllers\RegionCountryController;
use Modules\Region\Http\Public\Controllers\RegionController;
use Modules\Region\Http\Public\Controllers\RegionCityController;

use Modules\Region\Models\RegionCountry;
use Modules\Region\Models\Region;
use Modules\Region\Models\RegionCity;

Route::prefix('region')
    ->middleware('auth.basic.once', 'role:agent', 'api')
    ->where([
        'country' => '[0-9]+',
        'region' => '[0-9]+',
        'city' => '[0-9]+',
    ])
    ->group(function () {
        Route::model('country', RegionCountry::class);
        Route::model('region', Region::class);
        Route::model('city', RegionCity::class);

        Route::apiResource('country', RegionCountryController::class)->only(['index', 'show']);
        Route::apiResource('region', RegionController::class)->only(['index', 'show']);
        Route::apiResource('city', RegionCityController::class)->only(['index', 'show']);
    });
