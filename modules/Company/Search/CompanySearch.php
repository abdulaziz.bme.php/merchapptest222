<?php

namespace Modules\Company\Search;


use App\Search\Search;
use Illuminate\Database\Eloquent\Builder;

class CompanySearch extends Search
{
    public function __construct($only_active = false)
    {
    }

    protected array $relations = [
        'users',
    ];

    protected array $filterable = [
        'id'        => Search::FILTER_TYPE_EQUAL,
        'company_name'     => Search::FILTER_TYPE_LIKE,
        'is_active' => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $sortable = [
        'id'        => Search::SORT_TYPE_SIMPLE,
        'company_name'     => Search::SORT_TYPE_SIMPLE,
        'is_active' => Search::SORT_TYPE_SIMPLE,
    ];

    public function filter(array $params, string $combinedType, ?Builder $subQuery = null): Search
    {
        parent::filter($params, $combinedType, $subQuery);

        return $this;
    }
}
