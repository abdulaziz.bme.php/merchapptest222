<?php

use Illuminate\Support\Facades\Route;
use Modules\Company\Http\Admin\Controllers\CompaniesController;
use Modules\Company\Models\Company;

Route::where([
        'company' => '[0-9]+',
    ])
    ->group(function () {
        Route::model('companies', Company::class);

        Route::apiResource('companies', CompaniesController::class)->parameters([
            'companies' => 'companies',
        ]);
    });
