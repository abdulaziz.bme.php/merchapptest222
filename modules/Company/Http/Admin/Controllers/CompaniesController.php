<?php

namespace Modules\Company\Http\Admin\Controllers;

use App\Http\Controllers\ApiResourceController;
use Modules\Company\Http\Admin\Requests\CompanyStoreRequest;
use Modules\Company\Models\Company;
use Modules\Company\Resources\CompanyResource;
use Modules\Company\Search\CompanySearch;

class CompaniesController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model: new Company(),
            search: new CompanySearch(),
            resourceClass: CompanyResource::class
        );
    }

    public function store(CompanyStoreRequest $request)
    {
        return $this->save($request, 201);
    }

    public function update(CompanyStoreRequest $request)
    {
        return $this->save($request, 200);
    }
}
