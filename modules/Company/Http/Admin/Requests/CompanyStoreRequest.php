<?php

namespace Modules\Company\Http\Admin\Requests;


use App\Http\Requests\ActiveFormRequest;
use Illuminate\Validation\Rule;
use Modules\Company\Models\Company;
use Modules\Product\Models\ProductBrand;
use Modules\Question\Models\QuestionGroup;

class CompanyStoreRequest extends ActiveFormRequest
{
    public function __construct()
    {
        return parent::__construct(
            model: new Company()
        );
    }

    public function rules()
    {
        return [
            'company_name' => 'required|string|max:255',
            'is_active'    => 'required|boolean',
        ];
    }
}
