<?php

namespace Modules\Report\Http\Public\Requests;

use App\Http\Requests\ActiveFormRequest;
use App\Rules\ExistsSoftDeleteRule;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Modules\Report\Enums\ReportEnums;
use Modules\Report\Models\Report;
use Modules\Shop\Models\Shop;
use Modules\Task\Models\TaskReport;

/**
 * @property array products
 * @property array brands
 */
class ReportRequest extends ActiveFormRequest
{
    protected array $ignoredModelFields = ['images_list'];
    protected array $fileFields         = [
        'images_list' => 'images',
    ];

    public function __construct()
    {
        return parent::__construct(
            model: new Report()
        );
    }

    public function rules()
    {
        $shop_id = $this->get('shop_id', 0);
        $shop = Shop::query()->where('id', $shop_id)->first();

        return [
            'task_report_id' => 'nullable|exists:task_report,id',

            'shop_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'shop', extraQuery: function ($query) {
                    $query->where('is_active', 1);
                }),
                // new ExistsSoftDeleteRule($this->model, 'shop', extraQuery: function ($query) {
                //     $query->where('agent_id', auth()->user()->id)
                //         ->where('is_active', 1);
                // }),
            ],

            'type' => [
                'required',
                Rule::in(array_keys(ReportEnums::types())),
            ],

            'date_period_type' => [
                'required',
                Rule::in(array_keys(ReportEnums::datePeriodTypes())),
            ],
            'date_period_from' => [
                'required',
                'date',
                'date_format:d.m.Y',
//                'before_or_equal:' . date('d.m.Y', strtotime('-6 days')),
            ],

            'location'   => 'required|array|size:2',
            'location.*' => 'required|numeric',

            'images_list'    => [
                Rule::requiredIf(!$this->model->exists && $this->type == 'ds'),
                'array',
            ],
            'images_list.*'  => 'file|max:4096|mimes:jpg,png,webp',
            'comment'        => $this->type == 'ds' ? 'required|string' : 'nullable|string',
            'shop_is_closed' => 'required|boolean',

            // DS

            'products'                => !$this->shop_is_closed && $this->type == 'ds' ? 'required|array' : 'array',
            'products.*.id'           => 'integer',
            'products.*.product_id'   => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product', extraQuery: function ($query) {
                    $query->where('is_active', 1);
                }),
            ],
            'products.*.variation_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product_variation'),
            ],
            'products.*.supplier_id'  => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'shop_supplier', extraQuery: function ($query) {
                    $query->where('is_active', 1);
                }),
            ],
            'products.*.quantity'     => 'required|integer|min:1',
            'products.*.price'        => [Rule::requiredIf($shop->price_is_required ?? false), 'integer'],

            // SR

            'brands'            => !$this->shop_is_closed && $this->type == 'sr' ? 'required|array' : 'array',
            'brands.*.id'       => 'integer',
            'brands.*.brand_id' => [
                'required',
                new ExistsSoftDeleteRule($this->model, 'product_brand'),
            ],
            'brands.*.quantity' => 'required|integer|min:1',
        ];
    }

    public function withValidator($validator)
    {
        if (!$validator->fails()) {
            $validator->after(function ($validator) {

                // Agent task report

                if ($this->task_report_id) {
                    $taskReport = TaskReport::query()->find($this->task_report_id);

                    if ($taskReport->task->agent_id != auth()->user()->id) {
                        $validator->errors()->add('task_report_id', __('rules.report.task_report_id.not_yours'));
                    }
                }

                // Products & brands

                switch ($this->type) {
                    case 'ds':
                        $this->products ??= [];

                        $products = array_map(function ($value) {
                            $newValue = [
                                $value['product_id'],
                                $value['variation_id'] ?? null,
                                $value['supplier_id'],
                            ];

                            return implode('.', $newValue);
                        }, $this->products);

                        if (array_unique($products) != $products) {
                            $validator->errors()->add('products', __('rules.report.products.unique_combinations'));
                        }

                        break;

                    case 'sr':
                        $this->brands ??= [];

                        $brands = Arr::pluck($this->brands, 'brand_id');

                        if (array_unique($brands) != $brands) {
                            $validator->errors()->add('brands', __('rules.report.brands.unique_combinations'));
                        }

                        break;
                }
            });
        }
    }

    protected function prepareForValidation()
    {
        parent::prepareForValidation();

        $this->merge([
            'location' => array_values((array)$this->location),
        ]);

        $taskReport = TaskReport::query()->find($this->task_report_id);

        if ($taskReport) {
            $this->merge([
                'shop_id'          => $taskReport->shop_id,
                'type'             => $taskReport->type,
                'date_period_type' => $taskReport->date_period_type,
            ]);
        }
    }

    protected function passedValidation()
    {
        parent::passedValidation();

        if (!$this->shop_is_closed) {
            $this->model->fillableRelations = [
                $this->model::RELATION_TYPE_ONE_MANY => [
                    'products' => $this->type == 'ds' ? $this->products : [],
                    'brands'   => $this->type == 'sr' ? $this->brands : [],
                ],
            ];
        }
    }
}
