<?php

namespace Modules\Report\Resources;

use App\Helpers\ImageHelper;
use App\Resources\JsonResource;
use Modules\Shop\Resources\ShopResource;
use Modules\Task\Resources\TaskReportResource;
use Modules\User\Resources\UserResource;

class ReportResource extends JsonResource
{
    public function toArray($request)
    {
        $date_period_from = $this->date_period_from->format('d.m.Y');
        $date_period_to = $this->date_period_to->format('d.m.Y');

        return [
            'id'                   => $this->id,
            'creator_id'           => $this->creator_id,
            'shop_id'              => $this->shop_id,
            'task_report_id'       => $this->task_report_id,
            'type'                 => $this->type,
            'number'               => "$this->type-$this->id",
            'date_period_type'     => $this->date_period_type,
            'date_period_from'     => $date_period_from,
            'date_period_to'       => $date_period_to,
            'date_period_interval' => "$date_period_from - $date_period_to",
            'date_period_week'     => $this->date_period_to->format('W'),
            'location'             => $this->location,
            'images_list'          => array_map(fn($value) => [
                'w_100'    => ImageHelper::thumbnail($value, 'widen', [100]),
                'w_500'    => ImageHelper::thumbnail($value, 'widen', [500]),
                'original' => asset($value),
            ], $this->images_list),
            'comment'              => $this->comment,
            'shop_is_closed'       => $this->shop_is_closed,
            'created_at'           => $this->created_at->format('d.m.Y H:i:s'),
            'updated_at'           => $this->updated_at->format('d.m.Y H:i:s'),
            'is_deleted'           => (bool)$this->deleted_at,

            'creator'     => UserResource::make($this->whenLoaded('creator')),
            'shop'        => ShopResource::make($this->whenLoaded('shop')),
            'task_report' => TaskReportResource::make($this->whenLoaded('task_report')),
            'products'    => ReportProductResource::collection($this->whenLoaded('products')),
            'brands'      => ReportBrandResource::collection($this->whenLoaded('brands')),
        ];
    }
}
