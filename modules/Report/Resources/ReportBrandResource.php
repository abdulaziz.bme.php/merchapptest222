<?php

namespace Modules\Report\Resources;

use App\Resources\JsonResource;
use Modules\Product\Resources\ProductBrandResource;

class ReportBrandResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'quantity' => $this->quantity,
            'created_at' => $this->created_at->format('d.m.Y H:i:s'),
            'updated_at' => $this->updated_at->format('d.m.Y H:i:s'),

            'brand' => ProductBrandResource::make($this->whenLoaded('brand')),
        ];
    }
}
