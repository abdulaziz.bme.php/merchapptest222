<?php

namespace Modules\Report\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Modules\User\Models\User;
use Modules\Shop\Models\Shop;
use Modules\Task\Models\TaskReport;

class Report extends Model
{
    use SoftDeletes;

    protected $table = 'report';

    protected $fillable = [
        'creator_id',
        'shop_id',
        'task_report_id',
        'type',
        'date_period_type',
        'date_period_from',
        'location',
        'images_list',
        'comment',
        'shop_is_closed',
    ];

    protected $casts = [
        'date_period_from' => 'date:d.m.Y',
        'date_period_to' => 'date:d.m.Y',
        'location' => 'array',
        'images_list' => 'array',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id')->withTrashed();
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id')->withTrashed();
    }

    public function task_report()
    {
        return $this->belongsTo(TaskReport::class, 'task_report_id');
    }

    public function products()
    {
        return $this->hasMany(ReportProduct::class, 'report_id')->orderBy('sort_index');
    }

    public function brands()
    {
        return $this->hasMany(ReportBrand::class, 'report_id')->orderBy('sort_index');
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->date_period_from = Carbon::parse($model->date_period_from)->format('Y-m-d');
            $model->date_period_to = Carbon::parse($model->date_period_to)->format('Y-m-d');

            switch ($model->date_period_type) {
                case 'week':
                    $model->date_period_to = Carbon::parse($model->date_period_from)->addWeek()->subDay()->format('Y-m-d');
                    break;
                case 'month':
                    $model->date_period_to = Carbon::parse($model->date_period_from)->addMonth()->subDay()->format('Y-m-d');
                    break;
            }
        });
    }
}
