<?php

namespace Modules\Report\Models;

use App\Models\Model;
use Modules\Product\Models\ProductBrand;

class ReportBrand extends Model
{
    protected $table = 'report_brand';

    protected $fillable = [
        'brand_id',
        'quantity',
        'sort_index',
    ];

    public function report()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function brand()
    {
        return $this->belongsTo(ProductBrand::class, 'brand_id')->withTrashed();
    }
}
