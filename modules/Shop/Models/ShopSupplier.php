<?php

namespace Modules\Shop\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;
use Modules\Region\Models\RegionCountry;

class ShopSupplier extends Model
{
    use SoftDeletes;

    public const BM_ID = 53;

    protected $table = 'shop_supplier';

    protected $fillable = [
        'country_id',
        'short_name',
        'full_name',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id')->withTrashed();
    }

    public function country()
    {
        return $this->belongsTo(RegionCountry::class, 'country_id')->withTrashed();
    }

    public function shops()
    {
        return $this->belongsToMany(
            Shop::class,
            'shop_supplier_ref',
            'supplier_id',
            'shop_id',
        )->withTrashed();
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->id == self::BM_ID) {
                abort(403, __('errors.shop_supplier.undeleteable'));
            }
        });
    }
}
