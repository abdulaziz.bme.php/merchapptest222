<?php

namespace Modules\Shop\Resources;

use App\Resources\JsonResource;
use Modules\Product\Resources\ProductBrandResource;
use Modules\Question\Resources\QuestionGroupResource;
use Modules\Question\Resources\QuestionResource;
use Modules\Region\Resources\RegionCityResource;
use Modules\Report\Resources\ReportResource;
use Modules\User\Resources\UserResource;

class ShopResourceBrand extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this['name'],
            'id'=>$this['id'],
            'store_type' => $this->store_type,
        ];
    }
}
