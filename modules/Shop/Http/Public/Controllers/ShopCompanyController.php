<?php

namespace Modules\Shop\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;

use Modules\Shop\Models\ShopCompany;
use Modules\Shop\Search\ShopCompanySearch;
use Modules\Shop\Resources\ShopCompanyResource;

class ShopCompanyController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model: new ShopCompany(),
            search: new ShopCompanySearch(),
            resourceClass: ShopCompanyResource::class
        );
    }
}
