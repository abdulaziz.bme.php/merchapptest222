<?php

use Illuminate\Support\Facades\Route;

use Modules\Shop\Http\Public\Controllers\ShopSupplierController;
use Modules\Shop\Http\Public\Controllers\ShopContactController;
use Modules\Shop\Http\Public\Controllers\ShopController;
use Modules\Shop\Http\Public\Controllers\ShopCompanyController;

use Modules\Shop\Models\ShopSupplier;
use Modules\Shop\Models\ShopContact;
use Modules\Shop\Models\Shop;
use Modules\Shop\Models\ShopCompany;

Route::prefix('shop')
    ->middleware('auth.basic.once', 'role:agent', 'api')
    ->where([
        'supplier' => '[0-9]+',
        'contact' => '[0-9]+',
        'shop' => '[0-9]+',
        'company' => '[0-9]+',
    ])
    ->group(function () {
        Route::model('supplier', ShopSupplier::class);
        Route::model('contact', ShopContact::class);
        Route::model('shop', Shop::class);
        Route::model('company', ShopCompany::class);

        Route::apiResource('supplier', ShopSupplierController::class)->only(['index', 'show', 'store']);
        Route::apiResource('contact', ShopContactController::class)->only(['index', 'show', 'store']);
        Route::apiResource('shop', ShopController::class)->only(['index', 'show', 'store']);
        Route::apiResource('company', ShopCompanyController::class)->only(['index', 'show']);
    });
