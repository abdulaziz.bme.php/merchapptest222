<?php

namespace Modules\Shop\Http\Admin\Controllers;

use App\Http\Controllers\ApiResourceController;

use Illuminate\Support\Collection;
use Modules\Shop\Models\Shop;
use Modules\Shop\Resources\ShopResource2;
use Modules\Shop\Resources\ShopResourceBrand;
use Modules\Shop\Search\ShopSearch;
use Modules\Shop\Search\ShopMultipleSearch;
use Modules\Shop\Resources\ShopResource;
use Modules\Shop\Http\Admin\Requests\ShopRequest;

class ShopController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model:         new Shop(),
            search:        new ShopSearch(),
            resourceClass: ShopResource::class
        );
    }

    public function store(ShopRequest $request)
    {
        return $this->save($request, 201);
    }

    public function update(ShopRequest $request)
    {
        return $this->save($request, 200);
    }

    public function search()
    {
        parent::__construct(
            model:         new Shop(),
            search:        new ShopMultipleSearch(),
            resourceClass: ShopResource2::class
        );

        $paginator             = $this->search->queryBuilder->paginate($this->pageSize);
        $paginator->onEachSide = 0;

        return $this->resourceClass::collection($paginator)->response()->setStatusCode(206);
    }

    public function brand()
    {
        parent::__construct(
            model:         new Shop(),
            search:        new ShopMultipleSearch(),
            resourceClass: ShopResourceBrand::class
        );

        $paginator  = $this->search->queryBuilder->get()->unique('name')->pluck('name')->toArray();
        $paginator  = array_unique(array_map('strtolower', $paginator), SORT_STRING);
        $collection = new Collection();
        foreach ($paginator as $key => $item) {
            $data = [
                'name'       => ucfirst($item),
                'id'         => $key,
                'store_type' => 'OR',
            ];
            $collection->push($data);
        }

        return response()->json(
            [
                'data' => $collection,
            ],
        )->setStatusCode(206);
    }
}
