<?php

namespace Modules\Roles\Http\Admin\Requests;


use App\Http\Requests\ActiveFormRequest;
use Modules\Roles\Models\Role;

/**
 * @property array products
 * @property array brands
 */
class RoleRequest extends ActiveFormRequest
{
    protected array $ignoredModelFields = ['images_list'];
    protected array $fileFields         = [
        'images_list' => 'images',
    ];

    public function __construct()
    {
        return parent::__construct(
            model: new Role()
        );
    }

    public function rules()
    {
        return [
            'name'        => ['required', 'string', 'max:255', Rule::unique("roles")
                ->ignore($this->role->id)
                ->where("guard_name", $this->role->guard_name)],
            'guard_name'  => ['nullable', 'string', 'max:255'],
            'permissions' => ['nullable', 'array'],
        ];
    }

    protected function prepareForValidation()
    {
        parent::prepareForValidation();

        $this->merge([
            'permissions' => array_values((array)$this->permissions),
        ]);
    }

    protected function passedValidation()
    {
        parent::passedValidation();

        $this->model->fillableRelations = [
            $this->model::RELATION_TYPE_ONE_MANY => [
                'permissions' => $this->products,
            ],
        ];
    }
}
