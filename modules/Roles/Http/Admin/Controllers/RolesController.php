<?php

namespace Modules\Roles\Http\Admin\Controllers;

use App\Http\Controllers\ApiResourceController;
use Modules\Report\Http\Admin\Requests\ReportRequest;
use Modules\Report\Models\Report;
use Modules\Report\Resources\ReportResource;
use Modules\Report\Search\ReportSearch;
use Modules\Roles\Http\Admin\Requests\RoleRequest;
use Modules\Roles\Models\Role;
use Modules\Roles\Resources\RoleResource;
use Modules\Roles\Search\RoleSearch;

class RolesController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model: new Role(),
            search: new RoleSearch(),
            resourceClass: RoleResource::class
        );
    }

    public function store(RoleRequest $request)
    {
        return $this->save($request, 201);
    }

    public function update(RoleRequest $request)
    {
        return $this->save($request, 200);
    }
}
