<?php

namespace Modules\Stat\Http\Public\Middleware;

use Illuminate\Http\Request;

class PowerBIAuthenticateMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        $username = env('AUTHENTICATION_POWER_BI_USERNAME');
        $password = env('AUTHENTICATION_POWER_BI_PASSWORD');
        $authKey = 'Basic ' . base64_encode("$username:$password");

        if (request()->header('Authorization') !== $authKey) {
            abort(401, 'Invalid credentials.');
        }

        return $next($request);
    }
}
