<?php

use Illuminate\Support\Facades\Route;

use Modules\Stat\Http\Public\Middleware\PowerBIAuthenticateMiddleware;

use Modules\Stat\Http\Public\Controllers\PowerBIController;

Route::prefix('stat')
    ->group(function () {
        Route::prefix('power-bi')
            ->middleware(PowerBIAuthenticateMiddleware::class)
            ->group(function () {
                Route::get('product', [PowerBIController::class, 'product']);
            });
    });
