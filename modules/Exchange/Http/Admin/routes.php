<?php

use Illuminate\Support\Facades\Route;

use Modules\Exchange\Http\Admin\Controllers\Excel\ImportController;
use Modules\Exchange\Http\Admin\Controllers\Excel\Export\ExportProductController;
use Modules\Exchange\Http\Admin\Controllers\Excel\Export\ExportProductBrandController;
use Modules\Exchange\Http\Admin\Controllers\Excel\Export\ExportProductVariationController;

use Modules\Exchange\Http\Admin\Controllers\Excel\Export\Report\ExportReportDSController;
use Modules\Exchange\Http\Admin\Controllers\Excel\Export\Report\ExportReportSRController;

use Modules\Exchange\Http\Admin\Controllers\Excel\Export\Shop\ExportShopByCustomersController;
use Modules\Exchange\Http\Admin\Controllers\Excel\Export\Shop\ExportShopByRegionsController;
use Modules\Exchange\Http\Admin\Controllers\Excel\Export\Shop\ExportShopListController;

use Modules\Exchange\Http\Admin\Controllers\Excel\Export\ExportShopSupplierController;

Route::prefix('exchange')
    ->group(function () {
        Route::prefix('excel')
            ->group(function () {
                Route::prefix('import')
                    ->group(function () {
                        Route::post('product', [ImportController::class, 'product']);
                        Route::post('shop', [ImportController::class, 'shop']);
                    });

                Route::prefix('export')
                    ->group(function () {
                        Route::post('product', [ExportProductController::class, 'process']);
                        Route::post('product-brand', [ExportProductBrandController::class, 'process']);
                        Route::post('product-variation', [ExportProductVariationController::class, 'process']);

                        Route::prefix('report')
                            ->group(function () {
                                Route::post('ds', [ExportReportDSController::class, 'process']);
                                Route::post('sr', [ExportReportSRController::class, 'process']);
                            });

                        Route::prefix('shop')
                            ->group(function () {
                                Route::post('by-customers', [ExportShopByCustomersController::class, 'process']);
                                Route::post('by-regions', [ExportShopByRegionsController::class, 'process']);
                                Route::post('list', [ExportShopListController::class, 'process']);
                            });

                        Route::post('shop-supplier', [ExportShopSupplierController::class, 'process']);
                    });
            });
    });
