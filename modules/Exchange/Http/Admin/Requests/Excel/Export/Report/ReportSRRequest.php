<?php

namespace Modules\Exchange\Http\Admin\Requests\Excel\Export\Report;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class ReportSRRequest extends FormRequest
{
    public function rules()
    {
        return [
            'shop_id' => [
                'required',
                Rule::exists('shop', 'id'),
            ],
            'date' => 'required|date|date_format:d.m.Y',
        ];
    }
}
