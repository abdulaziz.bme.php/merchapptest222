<?php

namespace Modules\Exchange\Http\Admin\Requests\Excel\Import;

use App\Http\Requests\FormRequest;
use App\Services\LocalizationService;
use Illuminate\Support\Facades\DB;
use Modules\Exchange\Jobs\Excel\ImportJob;
use Modules\Product\Models\ProductCategory;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ProductRequest extends FormRequest
{
    public function rules()
    {
        return [
            'file' => 'required|file|max:102400|mimes:xlsx',
        ];
    }

    protected function passedValidation()
    {
        parent::passedValidation();

        DB::beginTransaction();

        try {
            $languages = LocalizationService::getInstance()->activeLanguages->pluck('code')->toArray();

            $file = $this->files->get('file');

            $spreadsheet = IOFactory::load($file->getPathname());
            $sheets = $spreadsheet->getAllSheets();

            foreach ($sheets as $sheet) {
                $categoryName = trim($sheet->getTitle());

                $category = ProductCategory::query();

                foreach ($languages as $language) {
                    $category->orWhere("name->$language", $categoryName);
                }

                $category = $category->first();

                if (!$category) {
                    throw new \Exception(__('errors.product_category.not_found', [
                        'category' => $categoryName,
                    ]));
                }

                $rows = $sheet->toArray();
                array_shift($rows);

                foreach ($rows as $key => $row) {
                    if (!$row) continue;

                    $row = array_map('trim', $row);
                    $record = new \stdClass();
                    $record->category_id = $category->id;
                    $record->model_number = $row[0];
                    $record->brandName = $row[1];
                    $record->variations = $row[2];
                    $record->date_eol = $row[3] ?? null;
                    $record->extra_id = $row[4] ?? "";

                    ImportJob::dispatch(
                        type     : ImportJob::TYPE_PRODUCT,
                        record   : $record,
                        creatorId: auth()->user()->id,
                    );
                }
            }

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }
}
