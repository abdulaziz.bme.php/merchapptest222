<?php

namespace Modules\Exchange\Http\Admin\Requests\Excel\Import;

use App\Http\Requests\FormRequest;
use Modules\Exchange\Jobs\Excel\ImportJob;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ShopRequest extends FormRequest
{
    public function rules()
    {
        return [
            'file' => 'required|file|max:102400|mimes:xlsx',
        ];
    }

    protected function passedValidation()
    {
        parent::passedValidation();

        $file = $this->files->get('file');

        $spreadsheet = IOFactory::load($file->getPathname());
        $sheet = $spreadsheet->getSheet(0);

        $rows = $sheet->toArray();
        array_shift($rows);

        foreach ($rows as $row) {
            if (!$row) continue;

            $row = array_map('trim', $row);

            $record = new \stdClass();
            $record->number = $row[0];
            $record->agentFullName = $row[1];
            $record->regionName = $row[2];
            $record->cityName = $row[3];
            $record->address = $row[4];
            $record->name = $row[5];
            $record->contactsFullNames = $row[6];
            $record->contactsPhones = $row[7];
            $record->store_type = $row[8];
            $record->focus_code = $row[9];
            $record->date = $row[10];

//            $record->has_credit_line = mb_strtolower($row[18]) === 'yes';
            $record->has_credit_line = false;

            $record->samsung_store_name = $row[12];
            $record->honor_store_name = $row[13];

            ImportJob::dispatch(
                type: ImportJob::TYPE_SHOP,
                record: $record,
                creatorId: auth()->user()->id,
            );
        }
    }
}
