<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export\Shop;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\Shop\ByCustomersRequest;
use Modules\Exchange\Services\Excel\Export\Shop\ByCustomersService;
use Modules\Region\Models\Region;
use Modules\Report\Models\ReportProduct;
use Modules\Shop\Models\Shop;
use Modules\Shop\Models\ShopSupplier;

class ExportShopByCustomersController extends ExportController
{
    public function process(ByCustomersRequest $request)
    {
        // Getting shops

        $shopsQuery = Shop::query()
            ->joinRelation('city.region')
            ->with([
                'contacts' => function ($q) {
                    $q->select(['full_name', 'phone']);
                },
            ])
            ->select([
                'shop.id',
                'shop.number as name',
                'shop.address',
                DB::raw("region.name->>'$this->locale' as region_name"),
                DB::raw("0 as quantity")
            ])
            ->orderBy('shop.number')
            ->withTrashed();

        if ($request->shop_ids) {
            $shopsQuery->whereIn('shop.id', $request->shop_ids);
        }

        $shops = $shopsQuery->get()->toArray();

        // Creating placeholders

        $suppliers = ShopSupplier::query()
            ->select([
                'id as supplier_id',
                DB::raw('shop_supplier.short_name as supplier_name'),
            ])
            ->getQuery()
            ->get()
            ->toArray();

        $suppliers = array_map(fn ($value) => (array)$value, $suppliers);

        $placeholders = Arr::crossJoin($shops, $suppliers);
        $placeholders = array_map(fn ($value) => array_merge(...$value), $placeholders);
        $placeholders = Arr::keyBy($placeholders, function ($value) {
            return $value['id'] . '_' . $value['supplier_id'];
        });

        // Getting report products quantity

        $reportProductQuantitiesQuery = ReportProduct::query()
            ->joinRelation('report')
            ->whereHas('report', function ($q) use ($request) {
                $q->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                    ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)));
            })
            ->select([
                DB::raw('report.shop_id as id'),
                DB::raw('report_product.supplier_id as supplier_id'),
                DB::raw('SUM(report_product.quantity) as quantity'),
            ])
            ->groupBy([
                'report.shop_id',
                'report_product.supplier_id',
            ]);

        if ($request->shop_ids) {
            $reportProductQuantitiesQuery->whereHas('report', function ($q) use ($request) {
                $q->whereIn('shop_id', $request->shop_ids);
            });
        }

        $reportProductQuantities = $reportProductQuantitiesQuery->getQuery()->get()->toArray();

        // Merging placeholders and report products quantities

        $reportProductQuantities = array_map(fn ($value) => (array)$value, $reportProductQuantities);
        $reportProductQuantities = Arr::keyBy($reportProductQuantities, function ($value) {
            return $value['id'] . '_' . $value['supplier_id'];
        });

        $shops = array_replace_recursive($placeholders, $reportProductQuantities);
        $shops = array_values($shops);

        $suppliers = null; // CLEARING VAR
        $placeholders = null; // CLEARING VAR
        $reportProductQuantities = null; // CLEARING VAR

        $spreadsheet = new Spreadsheet();

        $service = new ByCustomersService(
            spreadsheet: $spreadsheet,
            records: $shops,
        );

        $shops = null;

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
