<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export\Shop;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\Shop\ByRegionsRequest;
use Modules\Exchange\Services\Excel\Export\Shop\ByRegionsService;

use Modules\Region\Models\Region;
use Modules\Report\Models\Report;
use Modules\Shop\Models\Shop;
use Modules\Shop\Models\ShopSupplier;

class ExportShopByRegionsController extends ExportController
{
    public function process(ByRegionsRequest $request)
    {
        ShopSupplier::BM_ID;

        $regions = Region::query()->get(['id', DB::raw("name->'$this->locale' as name")]);

        $shops = [];

        foreach ($regions as $region) {
            $query = Report::query()
                ->where('type', $request->type)
                ->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)))
                ->whereHas('shop.city', function ($q) use ($region) {
                    $q->where('region_id', $region->id);
                })
                ->select([
                    DB::raw('COUNT(shop_id) as quantity'),
                ]);

            $shops[$region->id] = [
                'name' => $region->name,
                'quantity' => [
                    'total' => Shop::query()
                        ->whereHas('city', function ($q) use ($region) {
                            $q->where('region_id', $region->id);
                        })
                        ->count(),
                    'period_total' => (clone ($query))
                        ->first()
                        ->quantity,
                    'period_bm' => (clone ($query))
                        ->whereHas('products', function ($q) {
                            $q->where('supplier_id', ShopSupplier::BM_ID);
                        })
                        ->first()
                        ->quantity,
                ],
            ];
        }

        $spreadsheet = new Spreadsheet();

        $service = new ByRegionsService(
            spreadsheet: $spreadsheet,
            request: $request,
            records: array_values($shops),
        );

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
