<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Services\Excel\Export\ProductService;

use Modules\Product\Models\ProductCategory;

class ExportProductController extends ExportController
{
    public function process()
    {
        $categories = ProductCategory::query()->get(['id', DB::raw("name->'$this->locale' as name")]);

        $spreadsheet = new Spreadsheet();

        foreach ($categories as $key => $category) {
            $products = DB::table('product')
                ->join('product_brand', 'product_brand.id', '=', 'product.brand_id')
                ->where('category_id', $category->id)
                ->whereNull('product.deleted_at')
                ->select([
                    'product.id',
                    'product.model_number',
                    'product.date_eol',
                    'product.created_at',
                    'product_brand.name as brand_name',
                ])
                ->orderBy('product.id', 'DESC')
                ->get();

            $service = new ProductService(
                spreadsheet: $spreadsheet,
                sheet: [
                    'index' => $key,
                    'name' => $category->name,
                ],
                products: $products,
            );

            $service->process();
        }

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        $spreadsheet->removeSheetByIndex(count($categories));
        $spreadsheet->setActiveSheetIndex(0);

        return $this->responseExport($spreadsheet);
    }
}
