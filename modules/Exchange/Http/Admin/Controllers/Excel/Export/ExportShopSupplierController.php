<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel\Export;

use Modules\Exchange\Http\Admin\Controllers\Excel\ExportController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\DB;

use Modules\Exchange\Http\Admin\Requests\Excel\Export\ShopSupplierRequest;
use Modules\Exchange\Services\Excel\Export\ShopSupplierService;
use Modules\Report\Models\ReportProduct;

class ExportShopSupplierController extends ExportController
{
    public function process(ShopSupplierRequest $request)
    {
        $query = ReportProduct::query()
            ->joinRelation('supplier')
            ->joinRelation('report.shop.city.region')
            ->whereHas('report', function ($q) use ($request) {
                $q->where('date_period_from', '>=', date('Y-m-d', strtotime($request->date_from)))
                    ->where('date_period_from', '<=', date('Y-m-d', strtotime($request->date_to)));
            })
            ->select([
                DB::raw('shop_supplier.short_name as name'),
                DB::raw("region.name->>'$this->locale' as region_name"),
                DB::raw('SUM(report_product.quantity) as quantity'),
            ])
            ->groupBy([
                'shop_supplier.id',
                'region.id',
            ])
            ->orderBy('shop_supplier.short_name');

        $spreadsheet = new Spreadsheet();

        $service = new ShopSupplierService(
            spreadsheet: $spreadsheet,
            records: $query->get()->toArray(),
        );

        $service->process();

        $spreadsheet = $service->getSpreadsheet();

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            $sheet->setSelectedCell('A1');
        }

        return $this->responseExport($spreadsheet);
    }
}
