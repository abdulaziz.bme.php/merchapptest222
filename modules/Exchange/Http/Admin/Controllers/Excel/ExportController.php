<?php

namespace Modules\Exchange\Http\Admin\Controllers\Excel;

use App\Http\Controllers\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExportController extends Controller
{
    protected $locale;

    public function __construct()
    {
        $this->locale = app()->getLocale();
    }

    protected function responseExport(Spreadsheet $spreadsheet)
    {
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        ob_start();
        $writer->save("php://output");
        $fileContent = ob_get_contents();
        ob_end_clean();

        $response = [
            // 'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; base64," . base64_encode($fileContent),
            'file' => "data:application/vnd.ms-excel;base64," . base64_encode($fileContent),
        ];

        return response()->json($response, 200);
    }
}
