<?php

namespace Modules\Exchange\Services\Excel\Export\Shop;

use Illuminate\Support\Arr;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class ByCustomersService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private array $records,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $sheet = $this->spreadsheet->getSheet(0);
        $sheet->setTitle('Shops');

        // Data

        $sheet->setCellValueByColumnAndRow(1, 1, 'Store');
        $sheet->setCellValueByColumnAndRow(2, 1, 'Address');
        $sheet->setCellValueByColumnAndRow(3, 1, 'Salesman name');
        $sheet->setCellValueByColumnAndRow(4, 1, 'Salesman number');
        $sheet->setCellValueByColumnAndRow(5, 1, 'Area');
        $sheet->setCellValueByColumnAndRow(6, 1, 'TTL QTY');

        $suppliers = Arr::pluck($this->records, 'supplier_name');
        $suppliers = array_unique($suppliers);
        $suppliers = array_values($suppliers);

        foreach ($suppliers as $key => $supplier) {
            $startColumn = 7 + $key * 2;

            $sheet->setCellValueByColumnAndRow($startColumn, 1, "$supplier QTY");
            $sheet->setCellValueByColumnAndRow($startColumn + 1, 1, "Coverage $supplier");
        }

        $groupedRecords = [];

        foreach ($this->records as $value) {
            $shop = $value['name'];

            $groupedRecords[$shop] ??= [
                'name' => $value['name'],
                'address' => $value['address'],
                'contact_names' => implode("\n", Arr::pluck($value['contacts'], 'full_name')),
                'contact_phones' => implode("\n", Arr::pluck($value['contacts'], 'phone')),
                'region' => $value['region_name'],
                'suppliers' => [],
            ];

            $supplier = $value['supplier_name'];
            $groupedRecords[$shop]['suppliers'][$supplier] = $value['quantity'];
        }

        $groupedRecords = array_values($groupedRecords);

        $this->records = []; // CLEARING VAR

        foreach ($groupedRecords as $recordKey => $record) {
            $row = $recordKey + 2;

            $sheet->setCellValueByColumnAndRow(1, $row, $record['name']);
            $sheet->setCellValueByColumnAndRow(2, $row, $record['address']);

            $sheet->setCellValueByColumnAndRow(3, $row, $record['contact_names']);
            $sheet->getStyleByColumnAndRow(3, $row)->getAlignment()->setWrapText(true);
            $sheet->setCellValueExplicitByColumnAndRow(4, $row, $record['contact_phones'], DataType::TYPE_STRING);
            $sheet->getStyleByColumnAndRow(4, $row)->getAlignment()->setWrapText(true);

            $sheet->setCellValueByColumnAndRow(5, $row, $record['region']);

            $totalQuantityCells = [];

            foreach ($suppliers as $supplierKey => $supplier) {
                $startColumn = 7 + $supplierKey * 2;

                $totalQuantityCells[] = $sheet->getCellByColumnAndRow($startColumn, $row)->getCoordinate();

                $sheet->setCellValueByColumnAndRow($startColumn, $row, Arr::get($record, "suppliers.$supplier", 0));

                $percentageCellFormula = '=' . end($totalQuantityCells) . "/F$row";
                $sheet->setCellValueByColumnAndRow($startColumn + 1, $row, $percentageCellFormula);

                $sheet->getStyleByColumnAndRow($startColumn + 1, $row)
                    ->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);
            }

            $totalQuantityFormula = '=' . implode('+', $totalQuantityCells);
            $sheet->setCellValueByColumnAndRow(6, $row, $totalQuantityFormula);

            $row++;
        }

        $groupedRecords = null; // CLEARING VAR

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getDefaultRowDimension()->setRowHeight(-1);

        $sheet->getStyle("A1:{$maxColumn}1")->getFont()->setBold(true);

        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setVertical('center');

        $sheet->getStyle("A1:{$maxColumn}{$maxRow}")
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('666666'));

        $sheet->getStyle("B1:D$maxRow")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'ffff01'],
        ]);

        $sheet->getStyle("E1:E$maxRow")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'ffc100'],
        ]);

        $sheet->getStyle("F1:F$maxRow")->getFill()->applyFromArray([
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'fe0000'],
        ]);

        foreach ($suppliers as $key => $supplier) {
            if ($key % 2) {
                $startColumn = 7 + $key * 2;

                $sheet->getStyleByColumnAndRow($startColumn, 1, $startColumn + 1, $maxRow)->getFill()->applyFromArray([
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['rgb' => 'fff707'],
                ]);
            }
        }

        $suppliers = null; // CLEARING VAR

        return $this;
    }
}
