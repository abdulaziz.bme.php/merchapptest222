<?php

namespace Modules\Exchange\Services\Excel\Export\Shop;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ListService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private mixed $shops,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $locale = app()->getLocale();

        $sheet = $this->spreadsheet->getSheet(0);
        $sheet->setTitle('Shops');

        // Data

        $sheet->setCellValue('A1', __('fields.id'));
        $sheet->setCellValue('B1', __('fields.name'));
        $sheet->setCellValue('C1', __('fields.number'));
        $sheet->setCellValue('D1', __('fields.focus_code'));
        $sheet->setCellValue('E1', __('fields.address'));
        $sheet->setCellValue('F1', __('fields.agent_id'));
        $sheet->setCellValue('G1', __('fields.company_id'));
        $sheet->setCellValue('H1', __('fields.region_id'));
        $sheet->setCellValue('I1', __('fields.city_id'));
        $sheet->setCellValue('J1', __('fields.has_credit_line'));
        $sheet->setCellValue('K1', __('fields.is_active'));
        $sheet->setCellValue('L1', __('fields.last_report_date_period_interval'));
        $sheet->setCellValue('M1', __('fields.store_type'));

        foreach ($this->shops as $key => $shop) {
            $row = $key + 2;

            $datePeriodInterval = [
                date('d.m.Y', strtotime($shop->last_report->date_period_from)),
                date('d.m.Y', strtotime($shop->last_report->date_period_to)),
            ];

            $sheet->setCellValue("A$row", $shop->id);
            $sheet->setCellValue("B$row", $shop->name);
            $sheet->setCellValue("C$row", $shop->number);
            $sheet->setCellValue("D$row", $shop->focus_code);
            $sheet->setCellValue("E$row", $shop->address);
            $sheet->setCellValue("F$row", $shop->agent?->full_name);
            $sheet->setCellValue("G$row", $shop->company?->name);
            $sheet->setCellValue("H$row", $shop->city?->region->name[$locale]);
            $sheet->setCellValue("I$row", $shop->city?->name[$locale]);
            $sheet->setCellValue("J$row", $shop->has_credit_line ? __('information._common.yes') : __('information._common.no'));
            $sheet->setCellValue("K$row", $shop->is_active ? __('information._common.yes') : __('information._common.no'));
            $sheet->setCellValue("L$row", implode(' - ', $datePeriodInterval));
            $sheet->setCellValue("M$row", $shop->store_type);
        }

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getStyle("A1:{$maxColumn}1")->getFont()->setBold(true);

        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setVertical('center');

        return $this;
    }
}
