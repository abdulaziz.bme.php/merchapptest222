<?php

namespace Modules\Exchange\Services\Excel\Export\Report;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

class ReportSRService
{
    public function __construct(
        private Spreadsheet $spreadsheet,
        private array $report,
    ) {
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    public function process(): self
    {
        $sheet = $this->spreadsheet->getSheet(0);
        $sheet->setTitle('Report');

        // Data

        $brandsCount = count($this->report['brands']);

        $sheet->setCellValueByColumnAndRow(1, 1, __('fields.report_id'));
        $sheet->setCellValueByColumnAndRow(2, 1, __('fields.shop_id'));
        $sheet->setCellValueByColumnAndRow(3, 1, __('fields.focus_code'));
        $sheet->setCellValueByColumnAndRow(4, 1, __('fields.agent_id'));
        $sheet->setCellValueByColumnAndRow(5, 1, __('fields.region_id'));
        $sheet->setCellValueByColumnAndRow(6, 1, __('fields.city_id'));
        $sheet->setCellValueByColumnAndRow(7, 1, __('fields.created_at'));
        $sheet->setCellValueByColumnAndRow(8, 1, __('fields.brand_id'));

        $sheet->setCellValueByColumnAndRow(1, 2, $this->report['id']);
        $sheet->setCellValueByColumnAndRow(2, 2, $this->report['shop_number']);
        $sheet->setCellValueByColumnAndRow(3, 2, $this->report['shop_focus_code']);
        $sheet->setCellValueByColumnAndRow(4, 2, $this->report['creator_username']);
        $sheet->setCellValueByColumnAndRow(5, 2, $this->report['region_name']);
        $sheet->setCellValueByColumnAndRow(6, 2, $this->report['city_name']);
        $sheet->setCellValueByColumnAndRow(7, 2, $this->report['date']);

        $sheet->setCellValueByColumnAndRow(7, 3, 'Percent');
        $sheet->setCellValueByColumnAndRow(7, 4, 'Quantity');

        foreach ($this->report['brands'] as $key => $brand) {
            $column = 8 + $key;

            $sheet->setCellValueByColumnAndRow($column, 2, ' ' . $brand['brand']['name'] . ' ');
            $sheet->setCellValueByColumnAndRow($column, 4, $brand['quantity']);

            $brandQuantityCell = $sheet->getCellByColumnAndRow($column, 4)->getCoordinate();
            $totalQuantityCell = $sheet->getCellByColumnAndRow(8 + $brandsCount, 4)->getCoordinate();

            $sheet->setCellValueByColumnAndRow($column, 3, "=$brandQuantityCell/$totalQuantityCell");
            $sheet->getStyleByColumnAndRow($column, 3)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_PERCENTAGE);
        }

        $totalQuantityCellFrom = $sheet->getCellByColumnAndRow(8, 4)->getCoordinate();
        $totalQuantityCellTo = $sheet->getCellByColumnAndRow(8 + $brandsCount - 1, 4)->getCoordinate();
        $totalQuantityCellFormula = "=SUM($totalQuantityCellFrom:$totalQuantityCellTo)";

        $sheet->setCellValueByColumnAndRow(8 + $brandsCount, 1, 'Total');
        $sheet->setCellValueByColumnAndRow(8 + $brandsCount, 3, '100%');
        $sheet->setCellValueByColumnAndRow(8 + $brandsCount, 4, $totalQuantityCellFormula);

        $sheet->mergeCellsByColumnAndRow(8, 1, 8 + $brandsCount - 1, 1);
        $sheet->mergeCellsByColumnAndRow(8 + $brandsCount, 1, 8 + $brandsCount, 2);

        // Style

        $maxColumn = $sheet->getHighestDataColumn();
        $maxRow = $sheet->getHighestDataRow();

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }

        $sheet->getStyle("A1:{$maxColumn}1")->getFont()->setBold(true);

        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:' . $maxColumn . $maxRow)->getAlignment()->setVertical('center');

        $bordersRanges = [
            "A1:{$maxColumn}2",
            "G3:{$maxColumn}4",
        ];

        foreach ($bordersRanges as $bordersRange) {
            $sheet->getStyle($bordersRange)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(Border::BORDER_THIN)
                ->setColor(new Color('666666'));
        }

        return $this;
    }
}
