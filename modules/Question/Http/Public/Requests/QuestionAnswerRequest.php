<?php

namespace Modules\Question\Http\Public\Requests;

use App\Http\Requests\ActiveFormRequest;
use Illuminate\Validation\Rule;
use Modules\Question\Models\QuestionAnswer;

class QuestionAnswerRequest extends ActiveFormRequest
{
    public function __construct()
    {
        return parent::__construct(
            model: new QuestionAnswer()
        );
    }

    public function rules()
    {
        return [
            'shop_id'           => ['required', 'integer', Rule::exists('shop', 'id')],
            'report_id'         => ['nullable', 'integer', Rule::exists('report', 'id')],

            'answers'               => ['required', 'array'],
            'answers.*.question_id' => ['required', 'integer'],
            'answers.*.answer'      => ['required', 'string'],
        ];
    }

    protected function passedValidation()
    {
        parent::passedValidation();
    }
}
