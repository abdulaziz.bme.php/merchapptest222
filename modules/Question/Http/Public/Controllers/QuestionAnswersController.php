<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:03 PM
 */

namespace Modules\Question\Http\Public\Controllers;

use App\Http\Controllers\ApiResourceController;
use Modules\Question\Http\Public\Requests\QuestionAnswerRequest;
use Modules\Question\Models\QuestionAnswer;
use Modules\Question\Resources\QuestionAnswerResource;
use Modules\Question\Search\QuestionAnswerSearch;

class QuestionAnswersController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model        : new QuestionAnswer(),
            search       : new QuestionAnswerSearch(),
            resourceClass: QuestionAnswerResource::class
        );
    }

    public function store(QuestionAnswerRequest $request)
    {
        return $this->save($request, 201);
    }
}
