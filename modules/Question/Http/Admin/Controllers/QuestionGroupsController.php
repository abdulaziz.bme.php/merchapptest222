<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:03 PM
 */

namespace Modules\Question\Http\Admin\Controllers;

use App\Http\Controllers\ApiResourceController;
use Illuminate\Support\Facades\DB;
use Modules\Question\Enums\QuestionType;
use Modules\Question\Http\Admin\Requests\QuestionGroupRequest;
use Modules\Question\Models\Question;
use Modules\Question\Models\QuestionGroup;
use Modules\Question\Models\QuestionVariant;
use Modules\Question\Resources\QuestionGroupResource;
use Modules\Question\Search\QuestionGroupSearch;
use Modules\Shop\Models\Shop;

class QuestionGroupsController extends ApiResourceController
{
    public function __construct()
    {
        return parent::__construct(
            model:         new QuestionGroup(),
            search:        new QuestionGroupSearch(),
            resourceClass: QuestionGroupResource::class
        );
    }

    public function store(QuestionGroupRequest $request)
    {
        return $this->saveModel($request, 201);
    }

    public function update(QuestionGroupRequest $request)
    {
        return $this->saveModel($request, 200);
    }

    private function saveModel(QuestionGroupRequest $request, $status)
    {
        $request->model->fill($request->validated())->touch();

        Question::deleteOldest(
            $this->model->getKeyValues($request->get('questions', [])),
            'id',
            $this->model->questions());

        foreach ($request->get('questions', []) as $q) {
            if (count($q['variants'] ?? [])) {
                $q['question_type'] = QuestionType::VARIANT;
            } else {
                $q['question_type'] = QuestionType::TEXT;
            }

            $question = Question::query()->updateOrCreate([
                                                              'id' => $q['id'] ?? null,
                                                                                                                                                                                                                                                                                                        'group_id' => $request->model->id,
                                                          ], $q);


            QuestionVariant::deleteOldest(
                $this->model->getKeyValues(
                    $q['variants'] ?? []
                ),
                'id',
                $question->variants()
            );

            foreach ($q['variants'] ?? [] as $v) {
                QuestionVariant::query()->updateOrCreate([
                                                             'id' => $v['id'] ?? null,
                                                                                                                                                                                                                                                                                                       'question_id' => $question->id,
                                                         ], $v);
            }
        }
        if ($request->input('shop_type') == "ALL") {
            DB::table('question_group_shops')->where('group_id', $request->model->id)->delete();
            $shops = Shop::where([
                                     ['is_active', 1],
                                     ['deleted_at', null],
                                 ])->get();

            foreach ($shops as $shop) {
                DB::table('question_group_shops')->insert([
                                                              'group_id' => $request->model->id,
                                                              'shop_id'  => $shop->id,
                                                          ]);
            }
        } elseif ($request->input('shop_type') == "OR") {
            DB::table('question_group_shops')->where('group_id', $request->model->id)->delete();
            $shops = Shop::where([
                                     ['is_active', 1],
                                     ['deleted_at', null],
                                     ['store_type', 'OR'],
                                 ])->get();

            foreach ($shops as $shop) {
                DB::table('question_group_shops')->insert([
                                                              'group_id' => $request->model->id,
                                                              'shop_id'  => $shop->id,
                                                          ]);
            }
        } else {
            DB::table('question_group_shops')->where('group_id', $request->model->id)->delete();
            $shops = Shop::where([
                                     ['is_active', 1],
                                     ['deleted_at', null],
                                     ['store_type', 'IR'],
                                 ])->get();

            foreach ($shops as $shop) {
                DB::table('question_group_shops')->insert([
                                                              'group_id' => $request->model->id,
                                                              'shop_id'  => $shop->id,
                                                          ]);
            }
        }
        $request->model->refresh();

        $data = $this->resourceClass::make($request->model->withoutRelations());

        return response()->json($data, $status);
    }
}
