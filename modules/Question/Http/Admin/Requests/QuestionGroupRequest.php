<?php

namespace Modules\Question\Http\Admin\Requests;

use App\Http\Requests\ActiveFormRequest;
use Illuminate\Validation\Rule;
use Modules\Product\Models\ProductBrand;
use Modules\Question\Enums\VariantType;
use Modules\Question\Models\QuestionGroup;

class QuestionGroupRequest extends ActiveFormRequest
{
    public function __construct()
    {
        return parent::__construct(
            model: new QuestionGroup()
        );
    }

    public function rules()
    {
        return [
            'title'                               => 'required|string|max:255',
            'title_en'                            => 'required|string|max:255',
            'is_active'                           => 'required|boolean',
            'reusable'                            => 'required|boolean',
            'for_selected_shops'                  => 'required|boolean',
            'shop_type'                           => [Rule::requiredIf(function () {
                return $this->input('for_selected_shops', 0) == 1;
            })],
            'shops'                               => [Rule::requiredIf(function () {
                return $this->input('for_selected_shops', 0) == 1;
            }), 'array'],
            'shops.*'                             => [Rule::requiredIf(function () {
                return $this->input('for_selected_shops', 0) == 1;
            }), 'int'],
            'brand_id'                            => ['required', Rule::exists((new ProductBrand())->getTable(), 'id')],
            'questions'                           => ['required', 'array'],
            'questions.*.id'                      => ['integer'],
            'questions.*.question'                => ['required', 'string', 'max:255'],
            'questions.*.question_en'             => ['required', 'string', 'max:255'],
            'questions.*.variants'                => ['nullable', 'array'],
            'questions.*.variants.*.title'        => ['required', 'string', 'max:255'],
            'questions.*.variants.*.title_en'     => ['required', 'string', 'max:255'],
            'questions.*.variants.*.variant_type' => ['required', 'string', Rule::in([
                VariantType::No->value,
                VariantType::Yes->value,
            ])],

        ];
    }


    protected function passedValidation()
    {
        parent::passedValidation();

        $this->model->fillableRelations = [
            $this->model::RELATION_TYPE_MANY_MANY => [
                'shops' => $this->shops,
            ],
        ];
    }
}
