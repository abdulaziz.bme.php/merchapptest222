<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 2:37 PM
 */

use App\Http\Controllers\Actions\RestoreAction;
use Illuminate\Support\Facades\Route;
use Modules\Question\Http\Admin\Controllers\QuestionAnswerGroupsController;
use Modules\Question\Http\Admin\Controllers\QuestionGroupsController;
use Modules\Question\Models\QuestionAnswerGroup;
use Modules\Question\Models\QuestionGroup;

Route::prefix('questions')
    ->where([
        'groups'  => '[0-9]+',
        'answers' => '[0-9]+',
    ])
    ->group(function () {
        Route::model('groups', QuestionGroup::class);
        Route::model('answers', QuestionAnswerGroup::class);

        Route::apiResource('groups', QuestionGroupsController::class)->parameters([
            'groups' => 'groups',
        ]);
        Route::apiResource('answers', QuestionAnswerGroupsController::class)->parameters([
            'answers' => 'answers',
        ]);

        Route::delete('groups/{id}/restore', RestoreAction::class)->setBindingFields(['modelClass' => QuestionGroup::class]);

    });
