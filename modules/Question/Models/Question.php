<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:04 PM
 */

namespace Modules\Question\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\Question\Enums\QuestionType;

/**
 * @property int                          $id
 * @property int                          $group_id
 * @property string                       $question
 * @property string|Carbon                $created_at
 * @property string|Carbon                $updated_at
 * @property string|Carbon|null           $deleted_at
 * @property string                       $question_type
 * @property integer|null                 $parent_id
 *
 * @property QuestionAnswer[]|Collection  $answers
 * @property Question[]|Collection        $children
 * @property QuestionVariant[]|Collection $variants
 */
class Question extends Model
{
    use SoftDeletes;

    protected $table = 'questions';

    protected $fillable = [
        'group_id',
        'question',
        'question_en',
        'question_type',
    ];

    protected $casts = [
        'question_type' => QuestionType::class,
    ];

    /**
     * @return HasMany
     */
    public function answers()
    {
        return $this->hasMany(QuestionAnswer::class, 'question_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function group()
    {
        return $this->hasOne(QuestionGroup::class, 'id', 'group_id');
    }

    /**
     * @return HasMany
     */
    public function variants()
    {
        return $this->hasMany(QuestionVariant::class, 'question_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        self::deleted(function (self $model) {
            $model->answers()->delete();
        });

        self::restored(function (self $model) {
            $model = self::query()->onlyTrashed()->findOrFail($model->id);
            $model->restore();;
            QuestionAnswer::query()->onlyTrashed()->where('question_id', $model->id)->restore();
        });
    }
}
