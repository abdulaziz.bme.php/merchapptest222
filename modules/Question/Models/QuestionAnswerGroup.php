<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:04 PM
 */

namespace Modules\Question\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\Report\Models\Report;
use Modules\Shop\Models\Shop;

/**
 * @property int                         $id
 * @property int                         $shop_id
 * @property int                         $group_id
 * @property int                         $report_id
 * @property string|Carbon               $created_at
 * @property string|Carbon               $updated_at
 * @property string|Carbon|null          $deleted_at
 * @property string|null                 $extra_id
 * @property int                         $question_group_id
 *
 * @property Report                      $report
 * @property QuestionGroup               $group
 * @property Shop                        $shop
 * @property QuestionAnswer[]|Collection $answers
 */
class QuestionAnswerGroup extends Model
{
    use SoftDeletes;

    protected $table = 'question_answer_groups';

    protected $fillable = [
        'report_id',
        'shop_id',
    ];

    public function answers()
    {
        return $this->hasMany(QuestionAnswer::class, 'answer_group_id', 'id');
    }

    public function group()
    {
        return $this->hasOne(QuestionGroup::class, 'id', 'group_id');
    }

    public function report()
    {
        $this->hasOne(Report::class, 'id', 'report_id');
    }

    public function shop()
    {
        return $this->hasOne(Shop::class, 'id', 'shop_id');
    }

    protected static function boot()
    {
        parent::boot();

        self::deleted(function (self $model) {
            $model->answers()->delete();
        });
        self::restored(function (self $model) {
            QuestionAnswer::query()->onlyTrashed()->where('answer_group_id', $model->id)->restore();
        });
    }

}
