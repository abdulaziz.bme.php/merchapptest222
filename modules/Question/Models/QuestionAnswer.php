<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:04 PM
 */

namespace Modules\Question\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\Question\Enums\AnswerType;
use Modules\Question\Enums\VariantType;
use Modules\Report\Models\Report;
use Modules\Shop\Models\Shop;

/**
 * @property int                $id
 * @property int                $answer_group_id
 * @property int                $question_id
 * @property string             $answer
 * @property string|Carbon      $created_at
 * @property string|Carbon      $updated_at
 * @property string|Carbon|null $deleted_at
 * @property string             $answer_type
 * @property int                $question_group_id
 *
 * @property Question           $question
 */
class QuestionAnswer extends Model
{
    use SoftDeletes;

    protected $table = 'question_answers';

    protected $fillable = [
        'answer_group_id',
        'question_id',
        'answer',
        'answer_type',
        'question_group_id',
    ];

    protected $casts = [
        'answer_type' => AnswerType::class,
    ];

    protected $attributes = [
        'answer_type' => AnswerType::TEXT,
    ];

    public function answer(): Attribute
    {
        return Attribute::make(
            get: function ($val) {
                if ($this->answer_type === AnswerType::VARIANT) {
                    $variant = QuestionVariant::query()->where('id', intval($val))->first();
                    if ($variant) {
                        return $variant->title . ' (' . ($variant->variant_type->value == "Y" ? "Да" : "Нет") . ')';
                    }
                }
                return $val;
            },
        );
    }

    public function variant()
    {
        return $this->hasOne(QuestionVariant::class, 'id', 'answer');
    }

    public function question()
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }
}
