<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:19 PM
 */

namespace Modules\Question\Resources;

use App\Resources\JsonResource;

class QuestionVariantResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'question_id'  => $this->question_id,
            'title'        => $this->title,
            'title_en'     => $this->title_en,
            'variant_type' => $this->variant_type,
            'created_at'   => $this->created_at->format("Y-m-d H:i"),
            'updated_at'   => $this->updated_at->format("Y-m-d H:i"),
        ];
    }
}
