<?php
/**
 *
 * MerchApp-Web
 * User: Bakhodir @iambakhodir
 * Date: 25/03/23
 * Time: 3:18 PM
 */

namespace Modules\Question\Resources;

use App\Resources\JsonResource;
use Modules\Product\Resources\ProductBrandResource;
use Modules\Shop\Resources\ShopResource;

class QuestionGroupResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'title'              => $this->title,
            'title_en'           => $this->title_en,
            'is_active'          => $this->is_active,
            'reusable'           => $this->reusable,
            'for_selected_shops' => $this->for_selected_shops,
            'shop_type'          => $this->shop_type,
            'shops'              => ShopResource::collection($this->whenLoaded('shops')),
            'created_at'         => $this->created_at->format("Y-m-d H:i"),
            'updated_at'         => $this->updated_at->format("Y-m-d H:i"),
            'brand'              => ProductBrandResource::make($this->whenLoaded('brand')),
            'brand_id'           => $this->brand_id,
            'questions'          => QuestionResource::collection($this->whenLoaded('questions')),
        ];
    }
}
