<?php

namespace Modules\Question\Enums;

enum VariantType: string
{
    case Yes = "Y";
    case No  = "N";
}
