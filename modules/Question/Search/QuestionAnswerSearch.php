<?php

namespace Modules\Question\Search;

use App\Search\Search;

class QuestionAnswerSearch extends Search
{
    protected array $relations = [
        'question',
        'report',
        'shop',
    ];

    protected array $filterable = [
        'id'          => Search::FILTER_TYPE_EQUAL,
        'question_id' => Search::FILTER_TYPE_EQUAL,
        'answer'      => Search::FILTER_TYPE_LIKE,
    ];

    protected array $combinedFilterable = [];

    protected array $sortable = [
        'id'          => Search::SORT_TYPE_SIMPLE,
        'question_id' => Search::SORT_TYPE_SIMPLE,
        'answer'      => Search::SORT_TYPE_SIMPLE,
    ];
}
