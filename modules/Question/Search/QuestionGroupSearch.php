<?php

namespace Modules\Question\Search;

use App\Search\Search;
use Illuminate\Database\Eloquent\Builder;

class QuestionGroupSearch extends Search
{
    private $only_active;

    public function __construct($only_active = false)
    {
        $this->only_active = $only_active;
    }

    protected array $relations = [
        'questions',
        'brand',
        'shops',
    ];

    protected array $filterable = [
        'id'                 => Search::FILTER_TYPE_EQUAL,
        'title'              => Search::FILTER_TYPE_LIKE,
        'title_en'           => Search::FILTER_TYPE_LIKE,
        'is_active'          => Search::FILTER_TYPE_EQUAL,
        'reusable'           => Search::FILTER_TYPE_EQUAL,
        'for_selected_shops' => Search::FILTER_TYPE_EQUAL,
    ];

    protected array $sortable = [
        'id'        => Search::SORT_TYPE_SIMPLE,
        'title'     => Search::SORT_TYPE_SIMPLE,
        'title_en'  => Search::SORT_TYPE_SIMPLE,
        'is_active' => Search::SORT_TYPE_SIMPLE,
    ];

    public function filter(array $params, string $combinedType, ?Builder $subQuery = null): Search
    {
        parent::filter($params, $combinedType, $subQuery);

        if (isset($params['shop_id'])) {
            $shop_id = intval($params['shop_id']);

            $this->queryBuilder->where(function (Builder $q) use ($shop_id) {
                $q->where('for_selected_shops', 0)->orWhere(function (Builder $q) use ($shop_id) {
                    $q->where('for_selected_shops', 1)
                        ->whereHas('shops', function ($query) use ($shop_id) {
                            $query->where("shop.id", $shop_id);
                        });
                });
            });

            $this->queryBuilder->whereRaw('CASE reusable WHEN false THEN not exists(select * from question_answers left join question_answer_groups qag on question_answers.answer_group_id = qag.id where question_group_id = question_groups.id and qag.shop_id = ?) else true END', [$shop_id]);
        } else {
            $this->queryBuilder->where('for_selected_shops', 0);
        }

        return $this;
    }
}
