<?php

namespace Modules\User\Http\Admin\Controllers;

use App\Http\Controllers\ApiResourceController;
use Illuminate\Support\Facades\DB;
use Modules\Region\Models\RegionCity;
use Modules\User\Models\User;
use Modules\User\Search\UserSearch;
use Modules\User\Resources\UserResource;
use Modules\User\Http\Admin\Requests\UserRequest;

use Illuminate\Support\Arr;

class UserController extends ApiResourceController
{
    public function __construct()
    {
        parent::__construct(
            model:         new User(),
            search:        new UserSearch(),
            resourceClass: UserResource::class
        );

        $showQuery = (array)request()->get('show');
        $showQuery = Arr::flatten($showQuery);

        if (in_array('messages_count', $showQuery)) {
            $this->search->queryBuilder->withCount('messages');
        }
    }

    public function store(UserRequest $request)
    {
        $user_data = $this->save($request, 201);
        $user      = json_decode($user_data->content());
        $this->updateUserCityId($request,$user);
        return $user_data;
    }

    public function update(UserRequest $request)
    {
        $user_data = $this->save($request, 200);
        $user      = json_decode($user_data->content());
        $this->updateUserCityId($request,$user);
        return $user_data;
    }

    public function updateUserCityId($request,$user)
    {
        $array_check = [];
        if (is_array($request->regions) && count($request->regions) > 0) {
            //delete all cities
            DB::table('user_city_ref')->where('user_id', $user->id)->delete();
            // start reinserting cities
            foreach ($request->regions as $region) {
                $cities = RegionCity::query()->where('region_id', $region)->get()->pluck('id')->toArray();

                $intersect = array_intersect($request->cities ?? [], $cities);

                if (count($intersect) > 0) {
                    foreach ($intersect as $city) {
                        DB::table('user_city_ref')->insert(
                            [
                                'user_id' => $user->id,
                                'city_id' => $city,
                            ]);
                    }
                    $array_check[] = $intersect;
                } else {
                    foreach ($cities as $city) {
                        DB::table('user_city_ref')->insert(
                            [
                                'user_id' => $user->id,
                                'city_id' => $city,
                            ]);
                    }
                    $array_check[] = $cities;
                }
            }
        }

        //check other city which are not belongs to selected regions
        $last_city = array_diff($request->cities ?? [], Arr::collapse($array_check));
        // if exist insert those
        if (count($last_city) > 0) {
            foreach ($last_city as $city) {
                DB::table('user_city_ref')->insert(
                    [
                        'user_id' => $user->id,
                        'city_id' => $city,
                    ]);
            }
        }
    }
}
