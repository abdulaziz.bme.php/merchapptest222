<?php

namespace Modules\User\Http\Admin\Controllers;

use App\Http\Controllers\Controller;
use Modules\User\Resources\UserResource;
use Modules\User\Http\Admin\Requests\UserProfileRequest;

class UserProfileController extends Controller
{
    public function show()
    {
        $response = UserResource::make(auth()->user());
        return response()->json($response, 200);
    }

    public function update(UserProfileRequest $request)
    {
        $response = [];

        if ($request->new_password) {
            $response['token'] = 'Basic ' . base64_encode("$request->username:$request->new_password");
        }

        $request->model->fill($request->validated())->save();
        $request->model->refresh();

        $response['data'] = UserResource::make($request->model);

        return response()->json($response, 200);
    }

    public function getPermissions()
    {
        return auth()->check() ? auth()->user()->jsPermissions() : 0;
    }
}
