<?php

namespace Modules\User\Http\Public\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Models\User;
use Modules\User\Resources\UserResource;

class UserProfileController extends Controller
{
    public function show()
    {
        $response = UserResource::make(auth()->user());

        return response()->json($response, 200);
    }

    public function getShops($id, Request $request)
    {
        $shops = User::where('id', $id)->with(['shops'])->first();

        return response()->json($shops, 200);
    }
}
