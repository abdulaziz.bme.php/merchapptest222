<?php

return [
    '_common.invalid' => 'Неправильный :value',
    '_common.gte.numeric' => 'Значение поля :attribute должно быть :value или больше.',
    'exchange.report_export_dates_max_diff' => 'Максимальный диапазон - год.',
    'product_category.not_found' => "Категория ':category' не найдена",
    'product_specification.undeleteable' => 'Данная характеристика не подлежит удалению',
    'product_specification_option.delete_used_by_variation' => 'Невозможно удалить опцию ":option", так как она используется в вариации №:variation',
    'report.not_found' => 'Отчёт не найден',
    'report.no_brands' => 'В отчёте нет брендов',
    'shop_supplier.undeleteable' => 'Данный поставщик не подлежит удалению',
    'system_language.deactivate_main' => 'Невозможно деактивировать основной язык',
    'system_language.deactivate_current' => 'Невозможно деактивировать текущий язык',
    'task.agent_status_incompleted' => 'Заполните все отчёты для завершения задачи',
    'task_report.delete_used_by_report' => 'Невозможно удалить бланк отчёта №:taskReport, так как он используется в отчёте №:report',
    'user.undeleteable' => 'Данный пользователь не подлежит удалению',
];
