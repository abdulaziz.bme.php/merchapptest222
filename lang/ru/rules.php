<?php

return [
    '_common.unique' => 'Данное значение поля :attribute уже используется в записи №:pk',
    'product_variation.options.specification_single_option' => 'Каждая характеристика может иметь лишь одну опцию',
    'product_variation.options.unique_combinations_in_product' => 'Все комбинации опций внутри одного продукта должны быть уникальными',
    'report.products.unique_combinations' => 'Все комбинации продуктов, вариаций и поставщиков должны быть уникальны',
    'report.brands.unique_combinations' => 'Все бренды должны быть уникальны',
    'report.task_report_id.not_yours' => 'Данный бланк Вам не принадлежит.',
    'task.reports.unique_combinations' => 'Все комбинации магазинов, типов и временных периодов должны быть уникальны',
];
