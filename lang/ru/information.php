<?php

return [
    '_common.no' => 'Нет',
    '_common.yes' => 'Да',
    'exchange.import_started' => 'Импорт начался. Вы получите уведомление с результатами при его окончании.',
];
